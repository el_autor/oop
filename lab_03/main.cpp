#include "./interface/interface.h"

#define OK 0 

int main(int argc, char **argv)
{
    auto app = Gtk::Application::create(argc, argv, "");
    App window = App();

    app->run(*(window.get_window()));

    return OK;
}