make clean
mv ./Makefile ../
cd ..
make buildObject
mv ./*.o ./build
mv ./Makefile ./interface
cd interface
make buildObject
cp lab_03.glade ../build
mv ./*.o ../build
mv Makefile ../scene
cd ../scene
make buildObject
mv ./*.o ../build
mv Makefile ../commands
cd ../commands
make buildObject
mv ./*.o ../build
mv Makefile ../commandshandler
cd ../commandshandler
make buildObject
mv ./*.o ../build
mv Makefile ../scenecomposite
cd ../scenecomposite
make buildObject
mv ./*.o ../build
mv Makefile ../iterators
cd ../iterators
make buildObject
mv ./*.o ../build
mv Makefile ../exceptions
cd ../exceptions
make buildObject
mv ./*.o ../build
mv Makefile ../build
cd ../build
make app