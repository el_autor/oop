#include "transformmanagers.h"

TransformManager::TransformManager(Scene* scene) : BaseManager(scene){}

void TransformManager::transform(double delta1, double delta2, double delta3)
{

}

MoveManager::MoveManager(Scene* scene) : TransformManager(scene){}

void MoveManager::transform(double dx, double dy, double dz)
{
    std::shared_ptr<Composite> composite = std::static_pointer_cast<Composite>(_scene->getComposite());

    composite->shift(dx, dy, dz);
}

ScaleManager::ScaleManager(Scene* scene) : TransformManager(scene){}

void ScaleManager::transform(double kx, double ky, double kz)
{
    std::shared_ptr<Composite> composite = std::static_pointer_cast<Composite>(_scene->getComposite());

    composite->scale(kx, ky, kz);
}

RotateManager::RotateManager(Scene* scene) : TransformManager(scene){}

void RotateManager::transform(double angleX, double angleY, double angleZ)
{
    std::shared_ptr<Composite> composite = std::static_pointer_cast<Composite>(_scene->getComposite());

    composite->rotate(angleX, angleY, angleZ);
}