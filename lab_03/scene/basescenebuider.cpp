#include "basescenebuilder.h"

std::shared_ptr<Scene> BaseSceneBuilder::getScene()
{
    if (!scene)
    {
        scene = createScene();
    }

    return scene;
}