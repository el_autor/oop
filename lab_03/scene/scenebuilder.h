#ifndef _SCENEBUILDER_H_
#define _SCENEBUILDER_H_

#include <memory>
#include "scene.h"
#include "../scenecomposite/camera.h"
#include "../exceptions/scenebuilderexception.h"
#include "basescenebuilder.h"

class SceneBuilder : public BaseSceneBuilder
{
public:
    SceneBuilder(std::shared_ptr<BaseDrawer> drawer);

    virtual bool buildCamera() override;
    virtual bool buildComposite() override;

protected:
    virtual std::shared_ptr<Scene> createScene() override;

private:
    std::shared_ptr<BaseDrawer> _drawer;
    std::shared_ptr<BaseObject> _camera;
    std::shared_ptr<BaseObject> _composite;
    size_t chapter;

};

#endif