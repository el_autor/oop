#ifndef _GTK_CREATOR_H_
#define _GTK_CREATOR_H_

#include <gtkmm.h>
#include "basecreator.h"
#include "GTKdrawer.h"

class GTKCreator : public BaseCreator
{
public:
    GTKCreator(Gtk::DrawingArea* canvas, Cairo::RefPtr<Cairo::Context> context);

protected:
    virtual std::shared_ptr<BaseDrawer> createDrawer() override;

private:
    Gtk::DrawingArea* _canvas;
    Cairo::RefPtr<Cairo::Context> _cr;

};

#endif