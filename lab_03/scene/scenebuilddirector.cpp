#include "scenebuilddirector.h"

std::shared_ptr<Scene> SceneBuildDirector::create(std::shared_ptr<BaseSceneBuilder> builder)
{
    if (builder->buildCamera() &&
        builder->buildComposite())
    {
        return builder->getScene();
    }

    return std::shared_ptr<Scene>(new Scene());
}