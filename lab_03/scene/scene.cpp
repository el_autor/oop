#include "scene.h"
#include "transformmanagers.h"
#include "datamanagers.h"
#include "txtloadimplementor.h"
#include "txtloadmanager.h" 

Scene::Scene()
{

}

Scene::Scene(std::shared_ptr<BaseObject> copmosite,
             std::shared_ptr<BaseObject> camera,
             std::shared_ptr<BaseDrawer> newDrawer)
{
    complexObject = copmosite;
    curCamera = camera;
    drawer = newDrawer;
};

ObjectIterator Scene::begin()
{
    return complexObject->begin();
}

ObjectIterator Scene::end()
{
    return complexObject->end();
}

void Scene::addModel(std::shared_ptr<BaseObject> model)
{
    complexObject->addObject(model);
}

void Scene::removeModel(const size_t& index)
{
    //complexObject->removeObject();
}

void Scene::addCamera(std::shared_ptr<BaseObject> camera)
{
    complexObject->addObject(camera);
}

void Scene::removeCamera(const size_t& index)
{
    //complexObject->removeObject();
}

std::shared_ptr<BaseObject> Scene::getCurrentCamera()
{
    return curCamera;
}

std::shared_ptr<BaseObject> Scene::getComposite()
{
    return complexObject;
}

void Scene::move(double dx, double dy, double dz)
{
    MoveManager manager = MoveManager(this);

    manager.transform(dx, dy, dz);
}

void Scene::scale(double kx, double ky, double kz)
{
    ScaleManager manager = ScaleManager(this);

    manager.transform(kx, ky, kz);
}

void Scene::rotate(double angleX, double angleY, double angleZ)
{
    RotateManager manager = RotateManager(this);

    manager.transform(angleX, angleY, angleZ);
}

void Scene::load(const std::string& filename)
{   
    std::shared_ptr<BaseLoadImplementor> imp(new TxtLoadImplementor(this, filename));   
    TxtLoadManager manager(imp);

    manager.load();
}

void Scene::draw()
{
    DrawManager manager = DrawManager(this);

    manager.draw(drawer);
}
