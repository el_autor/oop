#include "GTKdrawer.h"
#include "iostream"

GTKDrawer::GTKDrawer()
{
    
}

GTKDrawer::GTKDrawer(Gtk::DrawingArea* canvas, Cairo::RefPtr<Cairo::Context> cr)
{
    _canvas = canvas;
    _cr = cr;
}

void GTKDrawer::drawLine(double xStart, double yStart, double xEnd, double yEnd)
{
    _cr->set_line_width(2.0);
    _cr->set_source_rgb(0.0, 0.0, 0.8);
    _cr->move_to(xStart, yStart);
    _cr->line_to(xEnd, yEnd);
    _cr->stroke();
}