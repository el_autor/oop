#include "txtloadimplementor.h"

TxtLoadImplementor::TxtLoadImplementor(Scene* scene,  const std::string& filename) : _filename(filename)
{
    _scene = scene;
};

void TxtLoadImplementor::load()
{
    std::shared_ptr<BaseModelBuilder> builder(new ModelBuilder(_filename));
    std::shared_ptr<ModelBuildDirector> director(new ModelBuildDirector());

    std::shared_ptr<Model> model = director->create(builder);

    if (model)
    {
        _scene->addModel(model);
    }
}