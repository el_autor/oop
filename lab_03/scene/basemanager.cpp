#include "basemanager.h"

BaseManager::BaseManager()
{

}

BaseManager::BaseManager(Scene* scene)
{
    _scene = scene;
}

void BaseManager::transform(double delta1, double delta2, double delta3)
{

}

void BaseManager::load()
{

}

void BaseManager::draw(std::shared_ptr<BaseDrawer> canvas)
{   

}