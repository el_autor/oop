#ifndef _TRANSFORM_MANAGER_H_
#define _TRANSFORM_MANAGER_H_

#include "basemanager.h"
#include "../scenecomposite/model.h"

class TransformManager : public BaseManager
{
public:
    TransformManager(Scene* scene);
    virtual void transform(double delta1, double delta2, double delta3) override;
};

class MoveManager : public TransformManager
{
public:
    MoveManager(Scene* scene);
    virtual void transform(double dx, double dy, double dz) override;
};

class ScaleManager : public TransformManager
{
public:
    ScaleManager(Scene* scene);
    virtual void transform(double kx, double ky, double kz) override;
};

class RotateManager : public TransformManager
{
public:
    RotateManager(Scene* scene);
    virtual void transform(double angleX, double angleY, double angleZ) override;
};

#endif