#ifndef _BASE_SCENE_BUILDER_H_
#define _BASE_SCENE_BUILDER_H_

#include "scene.h"

class BaseSceneBuilder
{
public:
    virtual bool buildCamera() = 0;
    virtual bool buildComposite() = 0;

    std::shared_ptr<Scene> getScene();

protected:
    virtual std::shared_ptr<Scene> createScene() = 0;

    std::shared_ptr<Scene> scene;

};

#endif