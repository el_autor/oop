#ifndef _SCENE_BUILD_DIRECTOR_H_
#define _SCENE_BUILD_DIRECTOR_H_

#include "basescenebuilder.h"

class SceneBuildDirector
{
public:
    std::shared_ptr<Scene> create(std::shared_ptr<BaseSceneBuilder> builder);

};

#endif