#ifndef _BASELOADMANAGER_H_
#define _BASELOADMANAGER_H_

#include "basemanager.h"
#include "baseloadimplementor.h"

class BaseLoadManager : public BaseManager
{
public:
    BaseLoadManager(std::shared_ptr<BaseLoadImplementor> imp);
    virtual void load() override;

protected:
    std::shared_ptr<BaseLoadImplementor> _imp;

};

#endif