#ifndef _TXTLOADMANAGER_H_
#define _TXTLOADMANAGER_H_

#include "baseloadmanager.h"
#include "txtloadimplementor.h"

class TxtLoadManager : public BaseLoadManager
{
public:
    TxtLoadManager(std::shared_ptr<BaseLoadImplementor> imp);
    virtual void load() override;

};

#endif