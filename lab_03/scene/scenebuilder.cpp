#include "scenebuilder.h"

SceneBuilder::SceneBuilder(std::shared_ptr<BaseDrawer> drawer)
{
    _drawer = drawer;
    chapter = 0;
}

bool SceneBuilder::buildCamera()
{
    _camera = std::shared_ptr<BaseObject>(new Camera());
    chapter = 1;

    return true;
}   

bool SceneBuilder::buildComposite()
{
    _composite = std::shared_ptr<BaseObject>(new Composite());
    chapter = 2;

    return true;
}

std::shared_ptr<Scene> SceneBuilder::createScene()
{
    if (chapter == 2)
    {
        scene = std::shared_ptr<Scene>(new Scene(_composite, _camera, _drawer));
    } 

    return scene;
}