#include "datamanagers.h"

DrawManager::DrawManager(Scene* scene) : BaseManager(scene){};

void DrawManager::draw(std::shared_ptr<BaseDrawer> drawer)
{
    std::shared_ptr<BaseObject> complexObject = _scene->getComposite();

    complexObject->draw(drawer);
}