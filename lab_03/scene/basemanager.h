#ifndef _BASE_MANAGER_H_
#define _BASE_MANAGER_H_

#include "scene.h"
#include <memory>
#include <string>

class BaseManager
{
protected:
    Scene* _scene;

public:
    BaseManager();
    BaseManager(Scene* scene);
    virtual void transform(double delta1, double delta2, double delta3);
    virtual void load();
    virtual void draw(std::shared_ptr<BaseDrawer> canvas);
};

#endif