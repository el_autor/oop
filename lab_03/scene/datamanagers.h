#ifndef _DATA_MANAGER_H_
#define _DATA_MANAGER_H_

#include "basemanager.h"
#include "../scenecomposite/modelbuilder.h"
#include "transformmanagers.h"
#include "../exceptions/loadexception.h"
#include <fstream>

class DrawManager : public BaseManager
{
public:
    DrawManager(Scene* scene);
    virtual void draw(std::shared_ptr<BaseDrawer> drawer) override;
};

#endif