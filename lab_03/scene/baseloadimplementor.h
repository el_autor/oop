#ifndef _BASELOADIMPLEMENTOR_H_
#define _BASELOADIMPLEMENTOR_H_

#include <string>
#include "scene.h"

class BaseLoadImplementor
{
public:
    virtual void load() = 0;

protected:
    Scene* _scene;
};

#endif