#include "txtloadmanager.h"

TxtLoadManager::TxtLoadManager(std::shared_ptr<BaseLoadImplementor> imp) : BaseLoadManager(imp){};

void TxtLoadManager::load()
{
    std::shared_ptr<TxtLoadImplementor> newImp = std::static_pointer_cast<TxtLoadImplementor>(_imp);
    newImp->load();
}
