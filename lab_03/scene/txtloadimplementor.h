#ifndef _TXTLOADIMPLEMENTOR_H_
#define _TXTLOADIMPLEMENTOR_H_

#include "baseloadimplementor.h"
#include "../scenecomposite/modelbuilddirector.h"

class TxtLoadImplementor : public BaseLoadImplementor
{
public:
    TxtLoadImplementor(Scene* scene, const std::string& filename);
    virtual void load() override;

private:
    std::string _filename;

};

#endif