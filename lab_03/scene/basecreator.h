#ifndef _BASE_CREATOR_H_
#define _BASE_CREATOR_H_

#include <memory>
#include "basedrawer.h"

class BaseCreator
{
public:
    std::shared_ptr<BaseDrawer> getDrawer();

protected:
    virtual std::shared_ptr<BaseDrawer> createDrawer() = 0;

private:
    std::shared_ptr<BaseDrawer> drawer;

};

#endif