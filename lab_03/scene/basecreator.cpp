#include "basecreator.h"

std::shared_ptr<BaseDrawer> BaseCreator::getDrawer()
{
    if (!drawer)
    {
        drawer = createDrawer();
    }

    return drawer;
}