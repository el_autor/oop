#ifndef _SCENE_H_
#define _SCENE_H_

#include <string>
#include <memory>
#include "../scenecomposite/baseobject.h"
#include "../scenecomposite/composite.h"
#include "GTKdrawer.h"

class Scene
{
public:
    Scene();
    Scene(std::shared_ptr<BaseObject> copmosite,
          std::shared_ptr<BaseObject> camera,
          std::shared_ptr<BaseDrawer> drawer);

    ObjectIterator begin();
    ObjectIterator end();

    void addModel(std::shared_ptr<BaseObject> model);
    void removeModel(const size_t& index);

    void addCamera(std::shared_ptr<BaseObject> camera);
    void removeCamera(const size_t& index);
    std::shared_ptr<BaseObject> getCurrentCamera();

    std::shared_ptr<BaseObject> getComposite();

    void setDrawer(std::shared_ptr<BaseDrawer> drawer);

    void move(double dx, double dy, double dz);
    void scale(double kx, double ky, double kz);
    void rotate(double angleX, double angleY, double angleZ);
    void load(const std::string& filename);
    void draw();

protected:
    std::shared_ptr<BaseObject> complexObject;
    std::shared_ptr<BaseObject> curCamera;
    std::shared_ptr<BaseDrawer> drawer;
};

#endif