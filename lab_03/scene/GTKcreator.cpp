#include "GTKcreator.h"

GTKCreator::GTKCreator(Gtk::DrawingArea* canvas, Cairo::RefPtr<Cairo::Context> context)
{
    _cr = context;
    _canvas = canvas;
}

std::shared_ptr<BaseDrawer> GTKCreator::createDrawer()
{
    return std::shared_ptr<BaseDrawer>(new GTKDrawer(_canvas, _cr));
}