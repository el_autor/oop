#ifndef _GTKDRAWER_H_
#define _GTKDRAWER_H_

#include "basedrawer.h"
#include <gtkmm.h>

class GTKDrawer : public BaseDrawer
{
public:
    GTKDrawer();
    GTKDrawer(Gtk::DrawingArea* canvas, Cairo::RefPtr<Cairo::Context> cr);

    virtual void drawLine(double xStart, double yStart, double xEnd, double yEnd);

private:
    Gtk::DrawingArea* _canvas;
    Cairo::RefPtr<Cairo::Context> _cr;

};

#endif