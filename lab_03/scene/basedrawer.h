#ifndef _BASEDRAWER_H_
#define _BASEDRAWER_H_

class BaseDrawer
{
public:
    virtual void drawLine(double xStart, double yStart, double xEnd, double yEnd) = 0;
};

#endif