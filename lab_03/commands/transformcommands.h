#ifndef _TRANSFORMCOMMANDS_H_
#define _TRANSFORMCOMMANDS_H_

#include "basecommand.h"
#include <math.h>

struct TransformParams
{
    double x, y, z;
};

class TransformCommand : public BaseCommand
{
protected:
    double _x, _y, _z;

public:
    TransformCommand(TransformParams params);
    TransformCommand(double x, double y, double z);
    virtual void processing(std::shared_ptr<Scene> scene) override;
};

class MoveCommand : public TransformCommand
{
public:
    MoveCommand(TransformParams params);
    MoveCommand(double x, double y, double z);
    virtual void processing(std::shared_ptr<Scene> scene) override;
};

class ScaleCommand : public TransformCommand
{
public:
    ScaleCommand(TransformParams params);
    ScaleCommand(double x, double y, double z);
    virtual void processing(std::shared_ptr<Scene> scene) override;        
};

class RotateCommand : public TransformCommand
{
public:
    RotateCommand(TransformParams params);
    RotateCommand(double x, double y, double z);
    virtual void processing(std::shared_ptr<Scene> scene) override;        
};

#endif