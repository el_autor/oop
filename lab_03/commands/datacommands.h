#ifndef _DATACOMMANDS_H_
#define _DATACOMMANDS_H_

#include <memory>
#include <string.h>
#include "basecommand.h"

class LoadCommand : public BaseCommand
{
private:
    std::string _filename;

public:
    LoadCommand(const std::string& filename);
    virtual void processing(std::shared_ptr<Scene> scene) override;
};

class DrawCommand : public BaseCommand
{
private:
    std::shared_ptr<BaseDrawer> _drawer;

public:
    DrawCommand();
    virtual void processing(std::shared_ptr<Scene> scene) override;
};

#endif