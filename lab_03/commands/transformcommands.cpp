#include "transformcommands.h"

TransformCommand::TransformCommand(TransformParams params)
{
    _x = params.x;
    _y = params.y;
    _z = params.z;
}

TransformCommand::TransformCommand(double x, double y, double z)
{
    _x = x;
    _y = y;
    _z = z;
}

void TransformCommand::processing(std::shared_ptr<Scene> scene){}

MoveCommand::MoveCommand(TransformParams params) : TransformCommand(params) {};
MoveCommand::MoveCommand(double x, double y, double z) : TransformCommand(x, y, z) {};

void MoveCommand::processing(std::shared_ptr<Scene> scene)
{
    scene->move(_x, _y, _z);
}

ScaleCommand::ScaleCommand(TransformParams params) : TransformCommand(params) {};
ScaleCommand::ScaleCommand(double x, double y, double z) : TransformCommand(x, y, z) {};

void ScaleCommand::processing(std::shared_ptr<Scene> scene)
{
    scene->scale(_x, _y, _z);   
}

RotateCommand::RotateCommand(TransformParams params) : TransformCommand(params) {};
RotateCommand::RotateCommand(double x, double y, double z) : TransformCommand(x, y, z)
{
    _x *= (M_PI / 180);
    _y *= (M_PI / 180);
    _z *= (M_PI / 180);
};

void RotateCommand::processing(std::shared_ptr<Scene> scene)
{
    scene->rotate(_x, _y, _z);
}