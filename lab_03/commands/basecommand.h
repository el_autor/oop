#ifndef _BASECOMMAND_H_
#define _BASECOMMAND_H_

#include "../scene/scene.h"

class BaseCommand
{
public:
    virtual ~BaseCommand() = 0;
    virtual void processing(std::shared_ptr<Scene> scene) = 0;
};

#endif