#include "datacommands.h"

LoadCommand::LoadCommand(const std::string& filename)
{
    _filename = filename;    
}

void LoadCommand::processing(std::shared_ptr<Scene> scene)
{
    scene->load(_filename);
}

DrawCommand::DrawCommand()
{

}

void DrawCommand::processing(std::shared_ptr<Scene> scene)
{
    scene->draw();
}