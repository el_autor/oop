#ifndef _BASEEXCEPTION_H_
#define _BASEEXCEPTION_H_

#include <exception>
#include <string>

class BaseException : public std::exception
{
public:
    BaseException(std::string msg);
    virtual const char* what() const noexcept override;

protected:
    std::string errMsg;

};

#endif