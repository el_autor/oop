#include "scenebuilderexception.h"

SceneBuilderException::SceneBuilderException() : BaseException(curErrMsg){}

SceneBuilderException::SceneBuilderException(std::string errDesc) : BaseException(curErrMsg + errDesc){}