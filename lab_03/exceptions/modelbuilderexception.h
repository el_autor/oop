#ifndef _MODEL_BUILDER_EXCEPTION_H_
#define _MODEL_BUILDER_EXCEPTION_H_

#include "baseexception.h"

class ModelBuilderException : public BaseException
{
private:
    static constexpr const char* curErrMsg = "Model builder error: ";

public:
    ModelBuilderException();
    ModelBuilderException(std::string msg);

};

#endif