#include "modelbuilderexception.h"

ModelBuilderException::ModelBuilderException() : BaseException(curErrMsg){}

ModelBuilderException::ModelBuilderException(std::string errDesc) : BaseException(curErrMsg + errDesc){}