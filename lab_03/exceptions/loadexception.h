#ifndef _INPUTEXCEPTION_H_
#define _INPUTEXCEPTION_H_

#include "baseexception.h"

class LoadException : public BaseException
{
private:
    static constexpr const char* curErrMsg = "Load data error: ";

public:
    LoadException();
    LoadException(std::string msg);

};

#endif