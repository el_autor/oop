#ifndef _SCENEBUILDEREXCEPTION_H_
#define _SCENEBUILDEREXCEPTION_H_

#include "baseexception.h"

class SceneBuilderException : public BaseException
{
private:
    static constexpr const char* curErrMsg = "Build scene error: ";

public:
    SceneBuilderException();
    SceneBuilderException(std::string msg);

};

#endif