#include "baseexception.h"

BaseException::BaseException(std::string msg) : errMsg(msg) {}

const char* BaseException::what() const noexcept
{
    return errMsg.c_str();
}