#include "loadexception.h"

LoadException::LoadException() : BaseException(curErrMsg){}

LoadException::LoadException(std::string errDesc) : BaseException(curErrMsg + errDesc){}
