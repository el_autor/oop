#ifndef _COMPOSITE_H_
#define _COMPOSITE_H_

#include <stdio.h>
#include <memory>
#include "baseobject.h"
#include "camera.h"

class Composite : public BaseObject
{
public:
    Composite();

    virtual ObjectIterator begin() override;
    virtual ObjectIterator end() override;

    virtual bool isComposite() override;
    virtual void addObject(std::shared_ptr<BaseObject> object) override;
    virtual void removeObject(ObjectIterator& iter) override;

    virtual void shift(double dx, double dy, double dz) override;
    virtual void scale(double kx, double ky, double kz) override;
    virtual void rotate(double angleX, double angleY, double angleZ) override;
    virtual void draw(std::shared_ptr<BaseDrawer> drawer) override;

    virtual bool isVisible() override;

private:
    void addMemory();

    std::shared_ptr<std::shared_ptr<BaseObject>[]> allObjects;
    size_t allocatedSize, objectsTotal;
};

#endif