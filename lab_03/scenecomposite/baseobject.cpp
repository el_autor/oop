#include "baseobject.h"

bool BaseObject::isComposite()
{
    return false;
}

bool VisibleObject::isVisible()
{
    return true;
}

bool InvisibleObject::isVisible()
{
    return false;
}