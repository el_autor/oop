#ifndef _BASEMODELBUILDER_H_
#define _BASEMODELBUILDER_H_

#include <fstream>
#include <memory>
#include "model.h"

class BaseModelBuilder
{
public:
    virtual bool buildNodes() = 0;
    virtual bool buildEdges() = 0;
    virtual bool buildModelCentre() = 0;
    
    std::shared_ptr<Model> getModel();

protected:
    virtual std::shared_ptr<Model> createModel() = 0;

    std::shared_ptr<Model> model;

};

#endif