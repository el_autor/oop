#ifndef _BASEOBJECT_H_
#define _BASEOBJECT_H_

#include <memory>
#include "../scene/basedrawer.h"

class ObjectIterator;

class BaseObject
{
public:
    virtual bool isVisible() = 0;
    virtual bool isComposite();
    virtual void addObject(std::shared_ptr<BaseObject> newObject) = 0;
    virtual void removeObject(ObjectIterator& iter) = 0;
    virtual ObjectIterator begin() = 0;
    virtual ObjectIterator end() = 0;
    virtual void shift(double dx, double dy, double dz) = 0;
    virtual void scale(double kx, double ky, double kz) = 0;
    virtual void rotate(double angleX, double angleY, double angleZ) = 0;
    virtual void draw(std::shared_ptr<BaseDrawer> drawer) = 0;

};

class VisibleObject : public BaseObject
{
public:
    virtual bool isVisible() override; 
};

class InvisibleObject : public BaseObject
{
public:
    virtual bool isVisible() override;
};

#endif