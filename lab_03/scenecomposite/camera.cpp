#include "camera.h"

Camera::Camera()
{
    shiftX = shiftY = shiftZ = 0;
    scaleX = scaleY = scaleZ = 1;
    rotateX = 15 * (M_PI / 180);
    rotateY = 30 * (M_PI / 180);
    rotateZ = 0;
}

bool Camera::isComposite()
{
    return false;
}

void Camera::addObject(std::shared_ptr<BaseObject> newObject)
{

}

void Camera::removeObject(ObjectIterator& iter)
{

}

ObjectIterator Camera::begin()
{
    return ObjectIterator();
}

ObjectIterator Camera::end()
{
    return ObjectIterator();
}

void Camera::shift(double dx, double dy, double dz)
{

}

void Camera::scale(double kx, double ky, double kz)
{

}

void Camera::rotate(double angleX, double angleY, double angleZ)
{

}

void Camera::draw(std::shared_ptr<BaseDrawer> drawer)
{
    
}