#ifndef _MODEL_H_
#define _MODEL_H_

#include <memory>
#include "baseobject.h"
#include "../iterators/baseiterator.h"
#include "node.h"
#include "edge.h"

class Model : public VisibleObject
{
public:
    Model();
    Model(std::shared_ptr<Node[]> nodes,
          std::shared_ptr<Edge[]> edges,
          Node modelCentre,
          size_t totalNodes, size_t totalEdges);

    virtual bool isComposite() override;
    virtual void addObject(std::shared_ptr<BaseObject> newObject) override;
    virtual void removeObject(ObjectIterator& iter) override;
    virtual ObjectIterator begin() override;
    virtual ObjectIterator end() override;
    virtual void shift(double dx, double dy, double dz) override;
    virtual void scale(double kx, double ky, double kz) override;
    virtual void rotate(double angleX, double angleY, double angleZ) override;
    virtual void draw(std::shared_ptr<BaseDrawer> drawer) override;

private:
    Node modelCentre;
    std::shared_ptr<Node[]> nodes;
    std::shared_ptr<Edge[]> edges;
    size_t totalNodes, totalEdges;

};

#endif