#include "modelbuilddirector.h"

std::shared_ptr<Model> ModelBuildDirector::create(std::shared_ptr<BaseModelBuilder> builder)
{
    if (builder->buildNodes() &&
        builder->buildEdges() &&
        builder->buildModelCentre())
    {
        return builder->getModel();
    }

    return std::shared_ptr<Model>(new Model());
}