#ifndef _MODELBUILDER_H_
#define _MODELBUILDER_H_

#include <fstream>
#include <memory>
#include "basemodelbuilder.h"
#include "../exceptions/modelbuilderexception.h"
#include "../exceptions/loadexception.h"
#include "node.h"
#include "edge.h"

/*
class ModelBuilder : public BaseModelBulder
{
public:
    ModelBuilder();
    virtual void build(std::ifstream& stream) override;
    virtual std::shared_ptr<Model> getModel() override;

    void buildNodes(std::ifstream& stream);
    void buildNode(double x, double y, double z);
    void buildEdges(std::ifstream& stream);
    void buildEdge(size_t start, size_t end);

    void calcModelCentre();

private:
    std::shared_ptr<Model> model;
    
};
*/

class ModelBuilder : public BaseModelBuilder
{
public:
    ModelBuilder(const std::string& filename);
    ~ModelBuilder();

    virtual bool buildNodes() override;
    virtual bool buildEdges() override;
    virtual bool buildModelCentre() override;

private:
    void buildNode(double x, double y, double z);
    void buildEdge(size_t startTop, size_t endTop);
    
    void openSrc(const std::string& filename);
    void closeSrc();

    void addNode(const Node& node);
    void addEdge(const Edge& edge);

    void addNodesMemory();
    void addEdgesMemory();

    Node modelCentre;
    std::shared_ptr<Node[]> nodes;
    std::shared_ptr<Edge[]> edges;
    size_t totalNodes, nodesAllocated;
    size_t totalEdges, edgesAllocated;

    std::ifstream stream;
    size_t chapter;

protected:
    virtual std::shared_ptr<Model> createModel() override;

};


#endif