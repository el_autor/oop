#ifndef _EDGE_H_
#define _EDGE_H_

#include <stdio.h>

class Edge
{
public:
    Edge();
    Edge(size_t startTop, size_t endTop);
    Edge(const Edge& edge);
    Edge(Edge&& edge);

    Edge& operator=(const Edge& edge);
    Edge& operator=(Edge&& edge);

    void setStart(size_t index);
    void setEnd(size_t index);

    size_t getStart();
    size_t getEnd();

private:
    size_t start, end;

};

#endif