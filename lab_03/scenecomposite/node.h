#ifndef _NODE_H_
#define _NODE_H_

#include <math.h>

class Node
{
public:
    Node();
    Node(double x, double y, double z);
    Node(const Node& node);
    Node(Node&& node);

    Node& operator=(const Node& node);
    Node& operator=(Node&& node);

    void setX(double x);
    void setY(double y);
    void setZ(double z);
    
    double getX();
    double getY();
    double getZ();

    void shift(double dx, double dy, double dz);
    void scale(double kx, double ky, double kz, Node centerNode);
    void rotate(double angleX, double angleY, double angleZ, Node centerNode);

private:
    double _x, _y, _z;
};

#endif