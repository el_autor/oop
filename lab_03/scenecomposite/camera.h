#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "baseobject.h"
#include "../iterators/baseiterator.h"
#include <math.h>

class Camera : public InvisibleObject
{
public:
    Camera();

    virtual bool isComposite() override;
    virtual void addObject(std::shared_ptr<BaseObject> newObject) override;
    virtual void removeObject(ObjectIterator& iter) override;
    virtual ObjectIterator begin() override;
    virtual ObjectIterator end() override;
    virtual void shift(double dx, double dy, double dz) override;
    virtual void scale(double kx, double ky, double kz) override;
    virtual void rotate(double angleX, double angleY, double angleZ) override;
    virtual void draw(std::shared_ptr<BaseDrawer> drawer) override;

private:

    double shiftX, shiftY, shiftZ,
           scaleX, scaleY, scaleZ,
           rotateX, rotateY, rotateZ;
};  

#endif