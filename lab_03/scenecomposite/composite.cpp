#include "composite.h"

Composite::Composite()
{
    allObjects = std::shared_ptr<std::shared_ptr<BaseObject>[]>(new std::shared_ptr<BaseObject>[5]);
    allocatedSize = 5;
    objectsTotal = 0;
}

bool Composite::isComposite()
{
    return true;
}

ObjectIterator Composite::begin()
{
    ObjectIterator iter(allObjects, objectsTotal, 0);

    return iter;
}

ObjectIterator Composite::end()
{
    ObjectIterator iter(allObjects, objectsTotal, objectsTotal);

    return iter;
}

void Composite::addObject(std::shared_ptr<BaseObject> object)
{
    if (objectsTotal < allocatedSize)
    {
        allObjects[objectsTotal] = object;   
    }
    else
    {
        addMemory();
        allObjects[objectsTotal] = object;
    }

    objectsTotal++;
}

void Composite::removeObject(ObjectIterator& iterator)
{

}

void Composite::shift(double dx, double dy, double dz)
{
    for (ObjectIterator it = begin(); it != end(); ++it)
    {
        if (it->isVisible())
        {
            it->shift(dx, dy, dz);
        }
    }
}

void Composite::scale(double kx, double ky, double kz)
{
    for (ObjectIterator it = begin(); it != end(); ++it)
    {
        if (it->isVisible())
        {
            it->scale(kx, ky, kz);
        }
    }
}

void Composite::rotate(double angleX, double angleY, double angleZ)
{
    for (ObjectIterator it = begin(); it != end(); ++it)
    {
        if (it->isVisible())
        {
            it->rotate(angleX, angleY, angleZ);
        }
    }
}

void Composite::draw(std::shared_ptr<BaseDrawer> drawer)
{
    for (ObjectIterator it = begin(); it != end(); ++it)
    {
        if (it->isVisible())
        {
            it->draw(drawer);
        }
    }
}

void Composite::addMemory()
{
    std::shared_ptr<std::shared_ptr<BaseObject>[]> local(new std::shared_ptr<BaseObject>[allocatedSize + 5]);

    for (size_t i = 0; i < allocatedSize; i++)
    {
        local[i] = allObjects[i];
    }

    allObjects = local;
    allocatedSize += 5;
}

bool Composite::isVisible()
{
    return true;
}