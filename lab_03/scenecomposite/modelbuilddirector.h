#ifndef _MODEL_BUILD_DIRETOR_H_
#define _MODEL_BUILD_DIRETOR_H_

#include "modelbuilder.h"

class ModelBuildDirector
{
public:
    std::shared_ptr<Model> create(std::shared_ptr<BaseModelBuilder> builder);

};

#endif