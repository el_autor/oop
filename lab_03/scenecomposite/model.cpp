#include "model.h"

Model::Model()
{

}

Model::Model(std::shared_ptr<Node[]> nodes,
             std::shared_ptr<Edge[]> edges,
             Node modelCentre,
             size_t totalNodes, size_t totalEdges)
{
    this->nodes = nodes;
    this->edges = edges;
    this->modelCentre = modelCentre;
    this->totalNodes = totalNodes;
    this->totalEdges = totalEdges;
}

bool Model::isComposite()
{
    return false;
}

void Model::addObject(std::shared_ptr<BaseObject> newObject)
{

}

void Model::removeObject(ObjectIterator& iter)
{

}

ObjectIterator Model::begin()
{
    return ObjectIterator();
}   

ObjectIterator Model::end()
{
    return ObjectIterator();
}

void Model::shift(double dx, double dy, double dz)
{
    for (size_t i = 0; i < totalNodes; i++)
    {
        nodes[i].shift(dx, dy, dz);
    }    

    modelCentre.shift(dx, dy, dz);
}

void Model::scale(double kx, double ky, double kz)
{   
    for (size_t i = 0; i < totalNodes; i++)
    {
        nodes[i].scale(kx, ky, kz, modelCentre);
    }    
}

void Model::rotate(double angleX, double angleY, double angleZ)
{
    for (size_t i = 0; i < totalNodes; i++)
    {
        nodes[i].rotate(angleX, angleY, angleZ, modelCentre);
    }
}

void Model::draw(std::shared_ptr<BaseDrawer> drawer)
{
    for (size_t i = 0; i < totalEdges; i++)
    {
        drawer->drawLine(nodes[edges[i].getStart()].getX(),
                         nodes[edges[i].getStart()].getY(),
                         nodes[edges[i].getEnd()].getX(),
                         nodes[edges[i].getEnd()].getY());
    }
}
