#include "modelbuilder.h"

ModelBuilder::ModelBuilder(const std::string& filename)
{
    openSrc(filename);
    chapter = 0;

    nodes = std::shared_ptr<Node[]>(new Node[10]);
    nodesAllocated = 10;
    totalNodes = 0;

    edges = std::shared_ptr<Edge[]>(new Edge[10]);
    edgesAllocated = 10;
    totalEdges = 0;
}

ModelBuilder::~ModelBuilder()
{
    closeSrc();
}

void ModelBuilder::openSrc(const std::string& filename)
{
    stream.open(filename);

    if (!stream)
    {
        throw LoadException("File not exists");
    }
}

void ModelBuilder::closeSrc()
{
    try
    {
        stream.close();
    }
    catch (std::ifstream::failure& e)
    {
        throw LoadException("File close error");
    }
}

void ModelBuilder::addNodesMemory()
{
    std::shared_ptr<Node[]> newNodesMemory(new Node[nodesAllocated + 10]);
    
    for (size_t i = 0; i < totalNodes; i++)
    {
        newNodesMemory[i] = nodes[i];
    }
    
    nodes = newNodesMemory;
    nodesAllocated += 10;
}

void ModelBuilder::addEdgesMemory()
{
    std::shared_ptr<Edge[]> newEdgesMemory(new Edge[edgesAllocated + 10]);

    for (size_t i = 0; i < totalEdges; i++)
    {
        newEdgesMemory[i] = edges[i];
    }

    edges = newEdgesMemory;
    edgesAllocated += 10;
}

void ModelBuilder::addNode(const Node& node)
{
    if (totalNodes < nodesAllocated)
    {
        nodes[totalNodes] = node;
    }
    else
    {
        addNodesMemory();
        nodes[totalNodes] = node;
    }

    totalNodes++;
}

void ModelBuilder::addEdge(const Edge& edge)
{
    if (totalEdges < edgesAllocated)
    {
        edges[totalEdges] = edge;
    }
    else
    {
        addEdgesMemory();
        edges[totalEdges] = edge;
    }

    totalEdges++;
}

bool ModelBuilder::buildNodes()
{
    long long total = 0;
    double x = 0, y = 0, z = 0;

    stream >> total;

    if (total <= 0)
    {
        return false;
    }

    for (long long i = 0; i < total; i++)
    {
        stream >> x >> y >> z;
        buildNode(x, y, z);
    }

    chapter = 1;

    return true;    
}

bool ModelBuilder::buildEdges()
{
    long long total = 0;
    size_t startTop = 0, endTop = 0;

    stream >> total;
    
    if (total <= 0)
    {
        return false;
    }

    for (long long i = 0; i < total; i++)
    {
        stream >> startTop >> endTop;
        buildEdge(startTop, endTop);
    }

    chapter = 2;

    return true;
}

bool ModelBuilder::buildModelCentre()
{
    double maxX = nodes[0].getX(),
           minX = nodes[0].getX(),
           maxY = nodes[0].getY(),
           minY = nodes[0].getY(),
           maxZ = nodes[0].getZ(), 
           minZ = nodes[0].getZ();

    for (size_t i = 1; i < totalNodes; i++)
    {
        double localX = nodes[i].getX(),
               localY = nodes[i].getY(),
               localZ = nodes[i].getZ();
        
        if (localX > maxX)
        {
            maxX = localX;
        }
        else if (localX < minX)
        {
            minX = localX;
        }

        if (localY > maxY)
        {
            maxY = localY;
        }
        else if (localY < minY)
        {
            minY = localY;
        }

        if (localZ > maxZ)
        {
            maxZ = localZ;
        }
        else if (localZ < minZ)
        {
            minZ = localZ;
        }
    }

    modelCentre = {minX + (maxX - minX) / 2,
                   minY + (maxY - minY) / 2,
                   minZ + (maxZ - minZ) / 2};

    chapter = 3;

    return true;
}

void ModelBuilder::buildNode(double x, double y, double z)
{
    Node newNode(x, y, z);

    addNode(newNode);
}

void ModelBuilder::buildEdge(size_t startTop, size_t endTop)
{
    Edge newEdge(startTop, endTop);

    addEdge(newEdge);
}

std::shared_ptr<Model> ModelBuilder::createModel()
{
    if (chapter == 3)
    {
        model = std::shared_ptr<Model>(new Model(nodes, edges, modelCentre, totalNodes, totalEdges));
    }

    return model;
}