#include "edge.h"

Edge::Edge()
{
    start = end = 0;
}

Edge::Edge(size_t startTop, size_t endTop) : start(startTop), end(endTop){}

Edge::Edge(const Edge& edge)
{
    start = edge.start;
    end = edge.end;
}

Edge::Edge(Edge&& edge)
{
    start = edge.start;
    end = edge.end;
    edge.~Edge();
}

Edge& Edge::operator=(const Edge& edge)
{
    start = edge.start;
    end = edge.end;

    return *this;
}

Edge& Edge::operator=(Edge&& edge)
{
    start = edge.start;
    end = edge.end;
    edge.~Edge();

    return *this;
}

void Edge::setStart(size_t startTop)
{
    start = startTop;
}

void Edge::setEnd(size_t endTop)
{
    end = endTop;
}

size_t Edge::getStart()
{
    return start;
}

size_t Edge::getEnd()
{
    return end;
}