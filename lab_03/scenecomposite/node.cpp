#include "node.h"

Node::Node()
{
    _x = _y = _z = 0;
}

Node::Node(double x, double y, double z) : _x(x), _y(y), _z(z){}

Node::Node(const Node& node)
{
    _x = node._x;
    _y = node._y;
    _z = node._z;
}

Node::Node(Node&& node)
{
    _x = node._x;
    _y = node._y;
    _z = node._z;
    node.~Node();    
}

Node& Node::operator=(const Node& node)
{
    _x = node._x;
    _y = node._y;
    _z = node._z;

    return *this;
}

Node& Node::operator=(Node&& node)
{
    _x = node._x;
    _y = node._y;
    _z = node._z;
    node.~Node();

    return *this;
}

double Node::getX()
{
    return _x;
}

double Node::getY()
{
    return _y;
}

double Node::getZ()
{
    return _z;
}

void Node::setX(double x)
{
    _x = x;
}

void Node::setY(double y)
{
    _y = y;
}

void Node::setZ(double z)
{
    _z = z;
}

void Node::shift(double dx, double dy, double dz)
{
    _x += dx;
    _y += dy;
    _z += dz;    
}

void Node::scale(double kx, double ky, double kz, Node centerNode)
{
    double delta = centerNode.getX();

    _x = (_x - delta) * kx + delta;
    delta = centerNode.getY();
    _y = (_y - delta) * ky + delta;
    delta = centerNode.getZ();
    _z = (_z - delta) * kz + delta;
}

void Node::rotate(double angleX, double angleY, double angleZ, Node centerNode)
{
    double centerX = centerNode.getX(),
           centerY = centerNode.getY(),
           centerZ = centerNode.getZ();

    double tempX = _x;

    _x = centerX + (_x - centerX) * cos(angleZ) + (_y - centerY) * sin(angleZ);
    _y = centerY + (_y - centerY) * cos(angleZ) + (centerX - tempX) * sin(angleZ);

    double tempY = _y;

    _y = centerY + (_y - centerY) * cos(angleX) + (_z - centerZ) * sin(angleX);
    _z = centerZ + (_z - centerZ) * cos(angleX) + (centerY - tempY) * sin(angleX);
    
    double tempZ = _z;

    _z = centerZ + (_z - centerZ) * cos(angleY) + (_x - centerX) * sin(angleY);
    _x = centerX + (_x - centerX) * cos(angleY) + (centerZ - tempZ) * sin(angleY);
}