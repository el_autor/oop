#ifndef _COMMANDS_HANDLER_H_
#define _COMMANDS_HANDLER_H_

#include <memory>
#include "../scene/scenebuilddirector.h"
#include "../scene/scenebuilder.h"
#include "../commands/basecommand.h"

class CommandsHandler
{
private:
    std::shared_ptr<Scene> scene;

public:
    CommandsHandler(std::shared_ptr<BaseDrawer> drawer);
    void handle(BaseCommand& command);
};

#endif