#include "commandshandler.h"

CommandsHandler::CommandsHandler(std::shared_ptr<BaseDrawer> drawer)
{
    std::shared_ptr<BaseSceneBuilder> builder(new SceneBuilder(drawer));
    std::shared_ptr<SceneBuildDirector> director(new SceneBuildDirector());

    scene = director->create(builder);
}

void CommandsHandler::handle(BaseCommand& command)
{
    command.processing(scene);
}