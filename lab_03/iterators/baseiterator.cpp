#include "baseiterator.h"

ObjectIterator::ObjectIterator()
{
    _index = _size = 0;
}

ObjectIterator::ObjectIterator(std::shared_ptr<std::shared_ptr<BaseObject>[]> obj, size_t size, size_t index)
{
    objects = obj;
    _size = size;
    _index = index;
}

ObjectIterator& ObjectIterator::operator++()
{
    _index++;

    return *this;
}

BaseObject* ObjectIterator::operator->()
{
    if (_index >= _size)
    {
        //throw iterator error;
    }

    return (objects.lock()[_index]).get();
}

BaseObject& ObjectIterator::operator*()
{
    if (_index >= _size)
    {
        // throw iterator error
    }

    return *(objects.lock()[_index]).get();   
}

bool ObjectIterator::operator!=(ObjectIterator iter)
{
    if (_index != iter._index)
    {
        return true;
    } 

    return false;
}