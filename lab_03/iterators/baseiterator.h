#ifndef _BASE_ITERATOR_H_
#define _BASE_ITERATOR_H_

#include <memory>
#include "../scenecomposite/baseobject.h"

class ObjectIterator
{
private:    
    std::weak_ptr<std::shared_ptr<BaseObject>[]> objects;
    size_t _index, _size;

public:
    ObjectIterator();
    ObjectIterator(std::shared_ptr<std::shared_ptr<BaseObject>[]> obj, size_t size, size_t index);

    ObjectIterator& operator++();
    BaseObject* operator->();
    BaseObject& operator*();
    bool operator!=(ObjectIterator iter);

};

#endif