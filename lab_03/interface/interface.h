#pragma once

#include <gtkmm.h>
#include <iostream>
#include <exception>
#include <string.h>
#include "../commands/datacommands.h"
#include "../commands/transformcommands.h"
#include "../commandshandler/commandshandler.h"
#include "../scene/GTKcreator.h"

using string = std::string;

class Signal
{
    private:

        sigc::signal<void, const string&> message_signal;

    public:

        sigc::signal<void, const string&> get_message_signal();

};

class App : public sigc::trackable
{
    private:

        Gtk::Window *mainWindow = nullptr;
        Gtk::Fixed *fixedGrid = nullptr;
        Gtk::Button *moveButton = nullptr;
        Gtk::Button *scaleButton = nullptr;
        Gtk::Button *rotateButton = nullptr;
        Gtk::Button *inputButton = nullptr;
        Gtk::Entry *dxEntry = nullptr;
        Gtk::Entry *dyEntry = nullptr;
        Gtk::Entry *dzEntry = nullptr;
        Gtk::Entry *kxEntry = nullptr;
        Gtk::Entry *kyEntry = nullptr;
        Gtk::Entry *kzEntry = nullptr;
        Gtk::Entry *filenameEntry = nullptr;
        Gtk::Entry *angleEntryX = nullptr;
        Gtk::Entry *angleEntryY = nullptr;
        Gtk::Entry *angleEntryZ = nullptr; 
        Gtk::DrawingArea *canvas = nullptr; 

        Glib::RefPtr<Gtk::Builder> _builder_;
        Signal signal;

        void on_moveButton_clicked();
        void on_scaleButton_clicked();
        void on_rotateButton_clicked();
        void on_inputButton_clicked();
        bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);
        void on_show_message(const string& message);
        void showMessageByError(std::exception& err);

        std::shared_ptr<CommandsHandler> handler;

        bool init;

    public:

        App();
        ~App();
        Gtk::Window*& get_window();
};