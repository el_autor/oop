#include "interface.h"

void App::showMessageByError(std::exception& err)
{
    signal.get_message_signal().emit(err.what());
}

sigc::signal<void, const string&> Signal::get_message_signal()
{
    return message_signal;
}

void App::on_show_message(const string& message)
{
    Gtk::MessageDialog dialog(*(this->mainWindow), "Ошибка!");

    dialog.set_secondary_text(message);
    dialog.run();
}

void App::on_moveButton_clicked()
{
    double dx = 0, dy = 0, dz = 0;

    try
    {
        dx = std::stod(dxEntry->get_text());
        dy = std::stod(dyEntry->get_text());
        dz = std::stod(dzEntry->get_text());

        MoveCommand command = MoveCommand(dx, dy, dz);
        handler->handle(command);
        canvas->queue_draw();
    }
    catch (std::exception& error)
    {
        showMessageByError(error);
    }
}

void App::on_scaleButton_clicked()
{
    double kx = 0, ky = 0, kz = 0;

    try
    {
        kx = std::stod(kxEntry->get_text());
        ky = std::stod(kyEntry->get_text());
        kz = std::stod(kzEntry->get_text());

        ScaleCommand command = ScaleCommand(kx, ky, kz);
        handler->handle(command);
        canvas->queue_draw();
    }
    catch (std::exception& error)
    {
        showMessageByError(error);
    }
}

void App::on_rotateButton_clicked()
{   
    double angleX = 0, angleY = 0, angleZ = 0;

    try
    {
        angleX = std::stod(angleEntryX->get_text());
        angleY = std::stod(angleEntryY->get_text());
        angleZ = std::stod(angleEntryZ->get_text());

        RotateCommand command = RotateCommand(angleX, angleY, angleZ);
        handler->handle(command);
        canvas->queue_draw(); 
    }
    catch (std::exception& error)
    {
        showMessageByError(error);
    }
}

void App::on_inputButton_clicked()
{
    string filename = filenameEntry->get_text();

    try
    {
        LoadCommand command = LoadCommand(filename);
        handler->handle(command);
        canvas->queue_draw();
    }
    catch (std::exception& error)
    {
        showMessageByError(error);
    }
}

bool App::on_draw(const Cairo::RefPtr<Cairo::Context>& context)
{
    if (!init)
    {
        std::shared_ptr<BaseCreator> creator(new GTKCreator(canvas, context));
        std::shared_ptr<BaseDrawer> drawer = creator->getDrawer();
        handler = std::shared_ptr<CommandsHandler>(new CommandsHandler(drawer));
        init = true;
    } 

    DrawCommand command = DrawCommand();
    handler->handle(command);

    return true;
}

App::App()
{
    _builder_ = Gtk::Builder::create();

    _builder_->add_from_file("./lab_03.glade");
    _builder_->get_widget("mainWindow", mainWindow);
    mainWindow->set_title("Lab_03");

    _builder_->get_widget("moveButton", moveButton);
    _builder_->get_widget("scaleButton", scaleButton);
    _builder_->get_widget("rotateButton", rotateButton);
    _builder_->get_widget("inputButton", inputButton);

    _builder_->get_widget("dxEntry", dxEntry);
    _builder_->get_widget("dyEntry", dyEntry);
    _builder_->get_widget("dzEntry", dzEntry);
    _builder_->get_widget("kxEntry", kxEntry);
    _builder_->get_widget("kyEntry", kyEntry);
    _builder_->get_widget("kzEntry", kzEntry);
    _builder_->get_widget("angleEntryX", angleEntryX);
    _builder_->get_widget("angleEntryY", angleEntryY);
    _builder_->get_widget("angleEntryZ", angleEntryZ);
    _builder_->get_widget("filenameEntry", filenameEntry);

    _builder_->get_widget("canvas", canvas);

    moveButton->signal_clicked().connect(sigc::mem_fun(*this, &App::on_moveButton_clicked));
    scaleButton->signal_clicked().connect(sigc::mem_fun(*this, &App::on_scaleButton_clicked));
    rotateButton->signal_clicked().connect(sigc::mem_fun(*this, &App::on_rotateButton_clicked));
    inputButton->signal_clicked().connect(sigc::mem_fun(*this, &App::on_inputButton_clicked));
    canvas->signal_draw().connect(sigc::mem_fun(*this, &App::on_draw));
    signal.get_message_signal().connect(sigc::mem_fun(*this, &App::on_show_message));

    init = false;
    canvas->queue_draw();
}

App::~App(){}

Gtk::Window*& App::get_window()
{
    return mainWindow;
}