#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

#include <QObject>
#include <QBoxLayout>
#include <memory>
#include "panelbutton.h"
#include "settings.h"
#include "direction.h"

enum class ControlPanelState
{
    FREE,
    WORKING
};

class ControlPanel : public QWidget
{
    Q_OBJECT
public:
    explicit ControlPanel(QWidget *parent = nullptr);
    ~ControlPanel();

signals:
    void sendNewTarget(int floor);

public slots:
    void slotAddNewTargetFloor(int targetFloor);
    void slotTargetFloorAchieved(int floor, MoveDirection direction);
    void slotReloadPosition(int floor, MoveDirection direction);

private:
    bool resetTargetFloor(int& floor);

    ControlPanelState state;
    int currentFloor;
    MoveDirection direction;

    bool floorsStatus[TOTAL_FLOORS];
    std::shared_ptr<PanelButton[]> buttons;

    QVBoxLayout *leftLayout, *rightLayout;
    QHBoxLayout* mainLayout;
};

#endif
