#ifndef CABIN_H
#define CABIN_H

#include <iostream>
#include <QObject>
#include <QTimer>
#include <QDebug>
#include "doors.h"
#include "direction.h"
#include "settings.h"

enum class CabinState
{
    MOVEMENT,
    BUSY,
    WAITING,
    FREE
};

class Cabin : public QObject
{
    Q_OBJECT
public:
    explicit Cabin(QObject *parent = nullptr);

signals:
    void floorTaskComplete(int floor, MoveDirection direction);
    void targetFloorAchieved();
    void movement();
    void sendPosition(int floor, MoveDirection direction);
    void doorsActivation();

public slots:
    void slotSetTarget(int floor);
    void slotMoveCabin();
    void slotArrival();
    void slotTaskComplete();

private:
    MoveDirection direction;
    int currentFloor, targetFloor;
    CabinState state;
    Doors doors;
    QTimer floorPassingTimer;
};

#endif
