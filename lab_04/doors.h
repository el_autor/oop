#ifndef DOORS_H
#define DOORS_H

#include <QObject>
#include <QTimer>
#include <iostream>
#include "settings.h"

enum class DoorsState
{
    OPENED,
    OPEN_PROCESS,
    CLOSED,
    CLOSE_PROCESS,
};

class Doors : public QObject
{
    Q_OBJECT
public:
    explicit Doors(QObject *parent = nullptr);

signals:
    void doorsAreClosed();

public slots:
    void slotOpenDoors();
    void slotCloseDoors();
    void slotDoorsAreOpened();
    void slotDoorsAreClosed();

private:
    DoorsState state;
    QTimer openProcessTimer, closeProcessTimer, waitingTimer;
};

#endif
