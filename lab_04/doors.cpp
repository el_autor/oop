#include "doors.h"

Doors::Doors(QObject *parent) : QObject(parent)
{
    state = DoorsState::CLOSED;

    QObject::connect(&openProcessTimer, SIGNAL(timeout()),
                     this, SLOT(slotDoorsAreOpened()));
    QObject::connect(&waitingTimer, SIGNAL(timeout()),
                     this, SLOT(slotCloseDoors()));
    QObject::connect(&closeProcessTimer, SIGNAL(timeout()),
                     this, SLOT(slotDoorsAreClosed()));
}

void Doors::slotOpenDoors()
{
    if (state == DoorsState::CLOSED)
    {
        state = DoorsState::OPEN_PROCESS;
        std::cout << "Doors are opening..." << std::endl;
        openProcessTimer.start(OPEN_PROCESS_TIME);
    }
    else if (state == DoorsState::CLOSE_PROCESS)
    {
        state = DoorsState::OPEN_PROCESS;
        std::cout << "Doors are opening..." << std::endl;
        int t = closeProcessTimer.remainingTime();
        closeProcessTimer.stop();
        openProcessTimer.start(OPEN_PROCESS_TIME - t);
    }
}

void Doors::slotCloseDoors()
{
    if (state == DoorsState::OPENED)
    {
        state = DoorsState::CLOSE_PROCESS;
        std::cout << "Doors are closing..." << std::endl;
        closeProcessTimer.start(CLOSE_PROCESS_TIME);
    }
}

void Doors::slotDoorsAreOpened()
{
    if (state == DoorsState::OPEN_PROCESS)
    {
        state = DoorsState::OPENED;
        std::cout << "Doors are opened!" << std::endl;
        waitingTimer.start(WAITING_TIME);
    }
}

void Doors::slotDoorsAreClosed()
{
    if (state == DoorsState::CLOSE_PROCESS)
    {
        state = DoorsState::CLOSED;
        std::cout << "Doors are closed!" << std::endl;
        emit doorsAreClosed();
    }
}
