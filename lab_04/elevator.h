#ifndef ELEVATOR_H
#define ELEVATOR_H

#include <QObject>
#include <vector>
#include "passenger.h"
#include "controlpanel.h"
#include "cabin.h"

class Elevator : public QObject
{
    Q_OBJECT
public:
    explicit Elevator(QObject *parent = nullptr);
    QWidget* getControllerWidget();

private:
    ControlPanel controller;
    Cabin cabin;
};

#endif // ELEVATOR_H
