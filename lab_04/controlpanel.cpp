#include "controlpanel.h"

ControlPanel::ControlPanel(QWidget *parent) : QWidget(parent)
{
    leftLayout = new QVBoxLayout;
    rightLayout = new QVBoxLayout;
    mainLayout = new QHBoxLayout;

    mainLayout->addLayout(leftLayout);
    mainLayout->addLayout(rightLayout);
    this->setLayout(mainLayout);

    state = ControlPanelState::FREE;
    buttons = std::shared_ptr<PanelButton[]>(new PanelButton[TOTAL_FLOORS]);

    for (int i = 0; i < TOTAL_FLOORS; i++)
    {
        buttons[i].setFloorNumber(i);
        buttons[i].setText(QString::number(i + 1));
        floorsStatus[i] = false;

        if (i % 2 == 0)
        {
            leftLayout->addWidget(dynamic_cast<QPushButton*>(&buttons[i]));
        }
        else
        {
            rightLayout->addWidget(dynamic_cast<QPushButton*>(&buttons[i]));
        }

        QObject::connect(&buttons[i], SIGNAL(requestFloor(int)),
                         this, SLOT(slotAddNewTargetFloor(int)));
    }
}

ControlPanel::~ControlPanel()
{
    delete leftLayout;
    delete rightLayout;
    delete mainLayout;
}

void ControlPanel::slotReloadPosition(int floor, MoveDirection curDirection)
{
    if (state == ControlPanelState::WORKING)
    {
        currentFloor = floor;
        direction = curDirection;
    }
}

void ControlPanel::slotAddNewTargetFloor(int targetFloor)
{
    state = ControlPanelState::WORKING;
    floorsStatus[targetFloor] = true;
    resetTargetFloor(targetFloor);
    emit sendNewTarget(targetFloor);
}

bool ControlPanel::resetTargetFloor(int& floor)
{
    int step = 0;

    if (direction == MoveDirection::UP)
    {
        step = 1;
    }
    else
    {
        step = -1;
    }

    for (int i = currentFloor; i < TOTAL_FLOORS && i >= 0; i += step)
    {
        if (floorsStatus[i])
        {
            floor = i;
            return true;
        }
    }

    step *= -1;

    for (int i = currentFloor; i < TOTAL_FLOORS && i >= 0; i += step)
    {
        if (floorsStatus[i])
        {
            floor = i;
            return true;
        }
    }

    return false;
}

void ControlPanel::slotTargetFloorAchieved(int floor, MoveDirection curDirection)
{
    if (state == ControlPanelState::WORKING)
    {
        direction = curDirection;
        currentFloor = floor;
        floorsStatus[floor] = false;

        emit buttons[floor].unpressPanelButton();

        if (resetTargetFloor(floor))
        {
            emit sendNewTarget(floor);
        }
        else
        {
            state = ControlPanelState::FREE;
        }
    }
}
