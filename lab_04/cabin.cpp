#include "cabin.h"

Cabin::Cabin(QObject *parent) : QObject(parent)
{
    state = CabinState::FREE;
    currentFloor = targetFloor = 0;
    direction = MoveDirection::STOPPAGE;

    QObject::connect(this, SIGNAL(movement()),
                     this, SLOT(slotMoveCabin()));

    QObject::connect(&floorPassingTimer, SIGNAL(timeout()),
                    this, SLOT(slotMoveCabin()));

    QObject::connect(this, SIGNAL(targetFloorAchieved()),
                     this, SLOT(slotArrival()));

    QObject::connect(this, SIGNAL(doorsActivation()),
                     &doors, SLOT(slotOpenDoors()));

    QObject::connect(&doors, SIGNAL(doorsAreClosed()),
                     this, SLOT(slotTaskComplete()));
}

void Cabin::slotArrival()
{
    state = CabinState::WAITING;
    std::cout << "Got " << currentFloor + 1 << " floor." << std::endl;
    emit doorsActivation();
}

void Cabin::slotTaskComplete()
{
    if (state == CabinState::WAITING)
    {
        state = CabinState::FREE;
        emit floorTaskComplete(currentFloor, direction); // направляем сигнал в панель управления
    }
}

void Cabin::slotMoveCabin()
{
    if (state == CabinState::BUSY)
    {
        state = CabinState::MOVEMENT;
        floorPassingTimer.start(FLOOR_PASSING_TIME);
    }
    else if (state == CabinState::MOVEMENT)
    {
        std::cout << "Pass " << currentFloor + 1 << " floor." << std::endl;

        if (direction == MoveDirection::UP)
        {
            currentFloor++;
        }
        else
        {
            currentFloor--;
        }

        emit sendPosition(currentFloor, direction);

        if (currentFloor == targetFloor)
        {
            emit targetFloorAchieved();
        }
        else
        {
            floorPassingTimer.start(FLOOR_PASSING_TIME);
        }
    }
}

void Cabin::slotSetTarget(int floor)
{
    if (state == CabinState::BUSY || state == CabinState::FREE)
    {
        state = CabinState::BUSY;
        targetFloor = floor;

        if (targetFloor == currentFloor)
        {
            emit targetFloorAchieved();
        }
        else
        {
            if (currentFloor < targetFloor)
            {
                direction = MoveDirection::UP;
            }
            else
            {
                direction = MoveDirection::DOWN;
            }

            emit movement();
        }
    }
}
