#ifndef DIRECTION_H
#define DIRECTION_H

enum class MoveDirection
{
    UP,
    DOWN,
    STOPPAGE
};

#endif
