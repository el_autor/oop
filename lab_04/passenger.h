#ifndef PASSENGER_H
#define PASSENGER_H

enum class PersonType
{
    CHILD,
    ADULT,
    ELDERLY
};

enum class PersonState
{
    INSIDE,
    OUTSIDE
};

class Passenger
{
public:
    explicit Passenger(PersonType curType, int floor);

private:
    PersonState state;
    PersonType type;
    int movingTime, targetFloor;
};

#endif
