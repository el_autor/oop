#include "panelbutton.h"

PanelButton::PanelButton(QWidget *parent) : QPushButton(parent)
{
    state = PanelButtonState::UNPRESSED;
    floorNumber = 0;

    QObject::connect(this, SIGNAL(clicked()),
                     this, SLOT(slotPressing()));
    QObject::connect(this, SIGNAL(unpressPanelButton()),
                     this, SLOT(slotSetUnpressed()));
}

PanelButton::~PanelButton(){}

void PanelButton::setFloorNumber(const int &number)
{
    floorNumber = number;
}

void PanelButton::slotPressing()
{
    if (state == PanelButtonState::UNPRESSED)
    {
        state = PanelButtonState::PRESSED;
        this->setEnabled(false);
        emit requestFloor(floorNumber);
    }
}

void PanelButton::slotSetUnpressed()
{
    if (state == PanelButtonState::PRESSED)
    {
        state = PanelButtonState::UNPRESSED;
        this->setEnabled(true);
    }
}
