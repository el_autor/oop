#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(300, 500);
    mainLayout = new QVBoxLayout;
    ui->centralWidget->setLayout(mainLayout);
    mainLayout->addWidget(elevator.getControllerWidget());
}

MainWindow::~MainWindow()
{
    delete mainLayout;
    delete ui;
}
