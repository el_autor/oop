#ifndef PANELBUTTON_H
#define PANELBUTTON_H

#include <QPushButton>
#include <QObject>

enum class PanelButtonState
{
    PRESSED,
    UNPRESSED
};

class PanelButton : public QPushButton
{
    Q_OBJECT
public:
    explicit PanelButton(QWidget *parent = nullptr);
    void setFloorNumber(const int& number);
    virtual ~PanelButton();

signals:
    void requestFloor(const int& floorNumber);
    void unpressPanelButton();

public slots:
    void slotPressing();
    void slotSetUnpressed();

private:
    PanelButtonState state;
    int floorNumber;
};

#endif
