#include "elevator.h"

Elevator::Elevator(QObject *parent) : QObject(parent)
{
    QObject::connect(&controller, SIGNAL(sendNewTarget(int)),
                     &cabin, SLOT(slotSetTarget(int)));

    QObject::connect(&cabin, SIGNAL(floorTaskComplete(int, MoveDirection)),
                     &controller, SLOT(slotTargetFloorAchieved(int, MoveDirection)));

    QObject::connect(&cabin, SIGNAL(sendPosition(int, MoveDirection)),
                     &controller, SLOT(slotReloadPosition(int, MoveDirection)));
}

QWidget *Elevator::getControllerWidget()
{
    return &controller;
}
