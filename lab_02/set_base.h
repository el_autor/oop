#pragma once

#include <cstddef>

class SetBase
{
public:

    virtual size_t getSize() const;
    virtual bool isEmpty() const;
    operator bool() const;
    virtual ~SetBase() = 0; 

protected:

    size_t size, allocatedSize;

};