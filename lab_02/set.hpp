#pragma once

#include "set.h"

template<typename Type>
Set<Type>& Set<Type>::operator=(const Set<Type>& set)
{
    elements = set.elements;
    size = set.size;
    allocatedSize = set.allocatedSize;

    return *this;
}

template<typename Type>
Set<Type>& Set<Type>::operator=(Set<Type>&& set)
{
    size = set.size;
    allocatedSize = set.allocatedSize;
    elements = set.elements;
    set.elements = nullptr;  

    return *this;  
}

template<typename Type>
Set<Type>& Set<Type>::operator=(const std::initializer_list<Type>& set)
{
    Set<Type> local(set);

    *this = local;

    return *this;
}

template<typename Type>
SetIterator<Type> Set<Type>::begin() const
{
    return SetIterator<Type>(elements, 0, size);
}

template<typename Type>
SetIterator<Type> Set<Type>::end() const
{
    return SetIterator<Type>(elements, size, size);
}

template<typename Type>
Set<Type>::Set() : elements(nullptr)
{   
    size = 0;
    allocatedSize = 0;
}

template<typename Type>
template<typename Iterator>
Set<Type>::Set(Iterator begin, Iterator end)
{
    size = 0;
    allocatedSize = 0;
    add(begin, end);
}

template<typename Type>
Set<Type>::Set(const Type* const arr, const size_t& arrSize)
{
    size = 0;
    allocatedSize = 0;

    for (size_t i = 0; i < arrSize; i++)
    {
        add(arr[i]);
    }
}

template<typename Type>
Set<Type>::Set(const std::initializer_list<Type>& args)
{
    size = 0, allocatedSize = 0;
    size_t argsSize = args.size();

    allocateMemory(argsSize);
    size = argsSize;
    allocatedSize = argsSize;
    Type* curElements = elements.get(); 

    for (size_t i = 0; i < argsSize; i++)
    {
        curElements[i] = *(std::begin(args) + i);
    }
}

template<typename Type>
Set<Type>::Set(const Set<Type>& set)
{   
    elements = set.elements;
    size = set.size;
    allocatedSize = set.allocatedSize;
}

template<typename Type>
Set<Type>::Set(Set<Type>&& set)
{
    *this = set;
}

template<typename Type>
void Set<Type>::allocateMemory(const size_t& sizeToAlloc)
{
    try
    {
        Type* elementsCopy = new Type[sizeToAlloc], *oldElements = elements.get();

        for (size_t i = 0; i < size; i++)
        {
            elementsCopy[i] = oldElements[i];
        }

        elements.reset(elementsCopy);
    }
    catch (std::bad_alloc& err)
    {
        std::cout << err.what() << std::endl;
    }
}

template<typename Type>
void Set<Type>::clear()
{
    elements = nullptr;
    size = allocatedSize = 0;
}

template<typename Type>
void Set<Type>::add(const Type& elem)
{
    if (elements.get() == nullptr)
    {
        allocateMemory(allocatedSize + 1);
        allocatedSize = 1;
    }
    else if (size == allocatedSize)
    {
        allocateMemory(allocatedSize * 2);
        allocatedSize *= 2;
    }
    
    insert(elem);
}

template<typename Type>
void Set<Type>::add(const Set<Type>& set)
{
    add(set.begin(), set.end());
}

template<typename Type>
void Set<Type>::add(const std::initializer_list<Type>& set)
{
    add(std::begin(set), std::end(set));    
}

template<typename Type>
template<typename Iterator>
void Set<Type>::add(Iterator begin, Iterator end)
{
    for (auto it = begin; it != end; it++)
    {
        add(*it);
    }
}

template<typename Type>
void Set<Type>::remove(const Type& elem)
{
    long long index = binarySearch(0, size - 1, elem);
    Type* curElements = elements.get();

    if (index != -1 && curElements[index] == elem)
    {
        for (size_t i = index; i < size - 1; i++)
        {
            curElements[i] = curElements[i + 1];
        }

        size--;
    }
}

template<typename Type>
void Set<Type>::remove(const Set<Type>& set)
{
    remove(set.begin(), set.end());
}

template<typename Type>
void Set<Type>::remove(const std::initializer_list<Type>& set)
{
    remove(std::begin(set), std::end(set));    
}

template<typename Type>
template<typename Iterator>
void Set<Type>::remove(Iterator begin, Iterator end)
{
    for (auto it = begin; it != end; it++)
    {
        remove(*it);
    }
}

template<typename Type>
bool Set<Type>::isExist(const Type& elem) const
{
    long long index = binarySearch(0, size - 1, elem);

    if (index >= 0 && *(this->begin() + index) == elem)
    {
        return true;
    }

    return false;
}

template<typename Type>
bool Set<Type>::isExist(const Set<Type>& set) const
{
    return isExist(set.begin(), set.end());
}

template<typename Type>
bool Set<Type>::isExist(const std::initializer_list<Type>& set) const
{
    return isExist(std::begin(set), std::end(set));
}

template<typename Type>
template<typename Iterator>
bool Set<Type>::isExist(Iterator begin, Iterator end)
{
    for (auto it = begin; it != end; it++)
    {
        if (!isExist(*it))
        {
            return false;
        }
    }

    return true;
}

template<typename Type>
void Set<Type>::insert(const Type& elem)
{
    long long index = binarySearch(0, size - 1, elem);    
    Type *curElements = elements.get();

    if (size == 0 || index == -1 || curElements[index] != elem)
    {
        size++;

        for (long long i = size - 1; i > index + 1; i--)
        {
            curElements[i] = curElements[i - 1];
        }

        if (index > 0 && (size_t)(index) == (size - 1))
        {
            curElements[index] = elem;
        }
        else
        {
            curElements[index + 1] = elem;
        }
    }
}

template<typename Type>
long long Set<Type>::binarySearch(const long long& low, const long long& high, const Type& elem) const
{
    if (high < low)
    {
        return low - 1;
    }

    long long mid = low + (high - low) / 2;
    Type* curElements = elements.get();

    if (curElements[mid] > elem)
    {
        return binarySearch(low, mid - 1, elem);
    }
    else if (curElements[mid] < elem)
    {
        return binarySearch(mid + 1, high, elem);
    }
    else
    {
        return mid;
    }
}

template<typename Type>
std::ostream& operator<<(std::ostream& stream, const Set<Type>& obj)
{
    stream << "{";

    for (SetIterator<Type> it = obj.begin(); it < obj.end() - 1; it++)
    {
        stream << " " << *(it) << " ,";
    }

    if (obj.begin() != obj.end())
    {
        stream << " " << *(obj.end() - 1) << " }";    
    }
    else
    {
        stream << "}"; 
    }

    return stream;
}

template<typename Type>
std::istream& operator>>(std::istream& stream, Set<Type>& obj)
{
    size_t totalNumber = 0;

    stream >> totalNumber;

    Type elem;

    for (size_t i = 0; i < totalNumber; i++)
    {
        stream >> elem;
        obj.add(elem);
    }

    return stream;
}

template<typename Type>
Set<Type> Set<Type>::_intersection_(SetIterator<Type> begin, SetIterator<Type> end) const
{
    Set<Type> resultSet;

    for (SetIterator<Type> it = begin; it != end; it++)
    {
        if (isExist(*it))
        {   
            resultSet.add(*it);
        }
    }

    return resultSet;    
}

template<typename Type>
Set<Type> Set<Type>::intersection(SetIterator<Type> begin, SetIterator<Type> end) const
{
    return _intersection_(begin, end);
}

template<typename Type>
Set<Type> Set<Type>::intersection(const Set<Type>& set) const
{
    return _intersection_(set.begin(), set.end());
}

template<typename Type>
Set<Type> Set<Type>::operator*(const Set<Type>& set) const
{
    return _intersection_(set.begin(), set.end());
}

template<typename Type>
Set<Type> Set<Type>::_unification_(SetIterator<Type> begin, SetIterator<Type> end) const
{
    Set<Type> resultSet;

    for (const Type& elem : *this)
    {
        resultSet.add(elem);
    }

    for (SetIterator it = begin; it != end; it++)
    {
        resultSet.add(*it);
    }

    return resultSet;
}

template<typename Type>
Set<Type> Set<Type>::unification(SetIterator<Type> begin, SetIterator<Type> end) const
{
    return _unification_(begin, end);
}

template<typename Type>
Set<Type> Set<Type>::unification(const Set<Type>& set) const
{
    return _unification_(set.begin(), set.end());
}
 
template<typename Type>
Set<Type> Set<Type>::operator+(const Set<Type>& set) const
{
    return _unification_(set.begin(), set.end());
}

template<typename Type>
Set<Type> Set<Type>::_difference_(SetIterator<Type> begin, SetIterator<Type> end) const
{
    Set<Type> resultSet;

    for (const Type& elem : *this)
    {
        resultSet.add(elem);
    }
   
    for (SetIterator<Type> it = begin; it != end; it++)
    {
        resultSet.remove(*it);
    }

    return resultSet;
}

template<typename Type>
Set<Type> Set<Type>::difference(SetIterator<Type> begin, SetIterator<Type> end) const
{
    return _difference_(begin. end);
}

template<typename Type>
Set<Type> Set<Type>::difference(const Set<Type>& set) const
{
    return _difference_(set.begin(), set.end());
}

template<typename Type>
Set<Type> Set<Type>::operator-(const Set<Type>& set) const
{
    return _difference_(set.begin(), set.end());    
}

template<typename Type>
bool Set<Type>::_equal_(SetIterator<Type> begin, SetIterator<Type> end) const
{
    if (this->getSize() != (size_t)(end - begin))
    {
        return false;
    }

    for (SetIterator<Type> it = begin; it != end; it++)
    {
        if (!this->isExist(*it))
        {
            return false;
        }
    }

    return true;
}

template<typename Type>
bool Set<Type>::equal(SetIterator<Type> begin, SetIterator<Type> end) const
{
    return _equal_(begin, end);
}

template<typename Type>
bool Set<Type>::equal(const Set<Type>& set) const
{
    return _equal_(set.begin(), set.end());
}

template<typename Type>
bool Set<Type>::operator==(const Set<Type>& set) const
{
    return _equal_(set.begin(), set.end());
}

template<typename Type>
bool Set<Type>::operator!=(const Set<Type>& set) const
{
    return !(_equal_(set.begin(), set.end()));
}

template<typename Type>
bool Set<Type>::_isSubSet_(SetIterator<Type> begin, SetIterator<Type> end) const
{
    Set<Type> local(begin, end);

    if (this->getSize() > local.size())
    {
        return false;
    }

    for (const Type& elem : *this)
    {
        if (!local.isExist(elem))
        {
            return false;
        }
    }

    return true;
}

template<typename Type>
bool Set<Type>::_isSuperSet_(SetIterator<Type> begin, SetIterator<Type> end) const
{
    if (this->getSize() < (size_t)(end - begin))
    {
        return false;
    }

    for (SetIterator<Type> it = begin; it != end; it++)
    {
        if (!isExist(*it))
        {
            return false;
        }
    }

    return true;    
}

template<typename Type>
bool Set<Type>::isSubSet(const Set<Type>& set) const
{
    return _isSubSet_(set.begin(), set.end());
}

template<typename Type>
bool Set<Type>::isSubSet(SetIterator<Type> begin, SetIterator<Type> end) const
{
    return _isSubSet_(begin, end);
}   

template<typename Type>
bool Set<Type>::isSuperSet(const Set<Type>& set) const
{
    return _isSuperSet_(set.begin(), set.end());
}

template<typename Type>
bool Set<Type>::isSuperSet(SetIterator<Type> begin, SetIterator<Type> end) const
{
    return _isSuperSet_(begin, end);
}

template<typename Type>
bool Set<Type>::operator<=(const Set<Type>& set) const
{
    return _isSubSet_(set.begin(), set.end());    
}

template<typename Type>
bool Set<Type>::operator>=(const Set<Type>& set) const
{
    return _isSuperSet_(set.begin(), set.end());    
}

template<typename Type>
bool Set<Type>::operator<(const Set<Type>& set) const
{
    return !(*this >= set);
}

template<typename Type>
bool Set<Type>::operator>(const Set<Type>& set) const
{
    return !(*this <= set);
}