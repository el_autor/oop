#include "exceptions.h"

ExceptionSet::ExceptionSet(const char* msg)
{
    size_t len = strlen(msg) + 1;

    this->errorMsg = new char[len];
    strcpy(this->errorMsg, msg);
}

ExceptionSet::~ExceptionSet()
{
    delete[] errorMsg;
}

const char* ExceptionSet::what() const noexcept
{
    return this->errorMsg;
}

IteratorSetException::IteratorSetException(const char* msg) : ExceptionSet(msg)
{
    size_t len = strlen(msg) + 1;

    this->iteratorErrorMsg = new char[len];
    strcpy(this->iteratorErrorMsg, msg);
}

IteratorSetException::~IteratorSetException()
{
    delete[] this->iteratorErrorMsg;
}

const char* IteratorSetException::what() const noexcept
{
    return this->iteratorErrorMsg;
}

IndexError::IndexError(const char* msg, const long long& errIndex) : IteratorSetException(msg), index(errIndex){}

IndexError::~IndexError(){}

const char* IndexError::what() const noexcept
{
    size_t len = strlen(errorMsg) + strlen(errIndexMsg) + 15;
    char* buffer = new char[len + 1];

    sprintf(buffer, "%s %s: %lli", errorMsg, errIndexMsg, index);  

    char* temp = errorMsg;
    delete[] temp;

    const_cast<IndexError*>(this)->errorMsg = buffer;

    return errorMsg;
}

EmptyIteratorError::EmptyIteratorError(const char* msg) : IteratorSetException(msg){}

EmptyIteratorError::~EmptyIteratorError(){};

const char* EmptyIteratorError::what() const noexcept
{
    size_t len = strlen(errorMsg) + strlen(emptyIterMsg) + 15;
    char* buffer = new char[len + 1];

    sprintf(buffer, "%s %s", errorMsg, emptyIterMsg);  

    char* temp = errorMsg;
    delete[] temp;

    const_cast<EmptyIteratorError*>(this)->errorMsg = buffer;

    return errorMsg;
}

WrongConvertionError::WrongConvertionError(const char* msg, const long long& result) : IteratorSetException(msg), errResult(result){}

WrongConvertionError::~WrongConvertionError(){};

const char* WrongConvertionError::what() const noexcept
{
    size_t len = strlen(errorMsg) + strlen(convertionMsg) + 15;
    char* buffer = new char[len + 1];

    sprintf(buffer, "%s %s: %lli", errorMsg, convertionMsg, errResult);  

    char* temp = errorMsg;
    delete[] temp;

    const_cast<WrongConvertionError*>(this)->errorMsg = buffer;

    return errorMsg;
}