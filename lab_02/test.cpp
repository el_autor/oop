#include <stdio.h>
#include "set.hpp"

#include <set>
#include <vector>

using namespace std;

int main()
{
    Set<int> obj;

    obj.add(1);
    obj.add(5);
    obj.add(10);
    obj.add(-1);
    obj.add(50);

    if (obj.isExist(2))
    {
        cout << "Exists" << endl;
    }   
    else
    {
        cout << "Not exists" << endl;
    }

    if (obj.isExist(10))
    {
        cout << "Exists" << endl;
    }   
    else
    {
        cout << "Not exists" << endl;
    }

    obj.add(100);

    cout << "0 - " << obj << endl;


    Set<int> obj1{1 ,2 ,3};

    obj1.add(10);
    obj1.add(6);
    cout << obj.getSize() << " - size" << endl;
    obj1.add(-1);
    cout << obj.getSize() << " - size" << endl;

    obj1.add(-5);
    obj1.add(40);
    obj1.add(3);    

    obj1.remove(-1);
    obj1.remove(-5);

    obj1.remove(-10);
    obj1.remove(100);
    obj1.remove(40);
    obj1.remove(3);

    obj1.add(-5);

    obj1.add(300);
    cout << "1 - " << obj1 << endl;

    cout << "delete check" << endl; 

    Set<int> obj2(obj1);
    cout << "copy object" << endl;
    cout << "2 - " << obj2 << endl;

    Set<int> obj3(obj + obj2);
    cout << "3 - " << obj3 << endl;

    //cout << obj2.count() << "- outside owners" << endl;
    Set<int> obj4(obj - obj2);
    //cout << obj2.count() << "- outside owners" << endl;
    cout << "4 - " << obj4 << endl;

    if (obj2 == obj1)
    {
        cout << "Equal sets" << endl;
    }
    else
    {
        cout << "Not equal sets" << endl;
    }

    if (obj2 == obj4)
    {
        cout << "Equal sets" << endl;
    }
    else
    {
        cout << "Not equal sets" << endl;
    }

    Set<int> obj5(obj2 * obj);

    cout << "5 - " << obj5 << endl;

    try
    {
        cout << *(obj5.begin()) << endl;
        cout << *(obj5.end() - 1) << endl;
        cout << *(obj5.end() - 1) << endl;
    }
    catch (exception& err)
    {
        cout << err.what() << endl;
    }

    SetIterator<int> it1 = obj5.begin();

    try
    {
        --it1;
        cout << *(it1) << endl;
        --it1;
        cout << *(it1) << endl;
        it1++;
        cout << *(it1) << endl;
        it1--;
        cout << *(it1) << endl;
    }
    catch (exception& err)
    {
        cout << err.what() << endl;
    }

    obj5 = obj1;

    cout << "5 - " << obj5 << endl;

    //Set<float> obj6;
    //cin >> obj6;
    //cout << obj6 << endl;
 
    set<int> sss = {1, 2, 3};
    sss.erase(0);

    auto it = begin(sss);
    cout << *it << endl;

    Set<int> obj10{1, 3, 4, 5};
    cout << obj10 << endl;

    cout << obj10.isEmpty() << endl;
    cout << obj10 << endl;
    obj10.clear();
    cout << obj10.isEmpty() << endl;
    cout << obj10 << endl;

    Set<int> obj11(begin(sss), end(sss));

    cout << obj11 << endl;    

    vector<int> vvv = {2, 2, 2, 5, 6, 7, 8, 8, 8, 10};

    Set<int> obj12(begin(vvv), end(vvv));

    cout << obj12 << endl;

    int* arr = new int[5];

    arr[0] = arr[1] = arr[2] = 1;
    arr[3] = 10, arr[4] = 20;

    Set<int> obj13(arr, 5);
    cout << obj13 << endl;

    delete[] arr;

    obj13.add(begin(vvv), end(vvv));

    try
    {
        size_t result = obj12.begin() - obj12.end();
    }
    catch (exception& err)
    {
        std::cout << err.what() << endl;
    }

    //size_t result = obj12.begin() - obj12.end();

    Set<int>* obj15 = new Set<int>();
    obj15->add({4, 6, 2});
    std::cout << *obj15 << std::endl; 

    SetIterator<int> iter15 = obj15->begin();
    std::cout << *iter15 << std::endl;

    delete obj15;
    
    try
    {
        std::cout << *iter15 << std::endl;
    }
    catch (exception& err)
    {
        std::cout << err.what() << endl;
    }

    return EXIT_SUCCESS;
}