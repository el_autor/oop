#pragma once

#include <exception>
#include <string.h>
#include <stdio.h>

class ExceptionSet : public std::exception
{
protected:

    char* errorMsg;

public:

    ExceptionSet(const char* msg);
    virtual ~ExceptionSet();
    virtual const char* what() const noexcept override;

};

class IteratorSetException : public ExceptionSet
{
private:

    char* iteratorErrorMsg;

public:

    IteratorSetException(const char* msg);
    virtual ~IteratorSetException();
    virtual const char* what() const noexcept override;

};

class IndexError : public IteratorSetException
{
private:

    const char* errIndexMsg = "Index out of range";
    long long index;

public:

    IndexError(const char* msg, const long long& index);
    virtual ~IndexError();
    virtual const char* what() const noexcept override;

};

class EmptyIteratorError : public IteratorSetException
{
private: 

    const char* emptyIterMsg = "Iterator has no object";

public:

    EmptyIteratorError(const char* msg);
    virtual ~EmptyIteratorError();
    virtual const char* what() const noexcept override;

};

class WrongConvertionError : public IteratorSetException
{
private: 

    const char* convertionMsg = "Invalid operands";
    long long errResult;

public:

    WrongConvertionError(const char* msg, const long long& result);
    virtual ~WrongConvertionError();
    virtual const char* what() const noexcept override;

};