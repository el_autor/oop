#include "set_base.h"

size_t SetBase::getSize() const
{
    return size;
}

SetBase::operator bool() const
{
    return (bool)size;
}

bool SetBase::isEmpty() const
{
    return bool(*this);
}

SetBase::~SetBase(){}