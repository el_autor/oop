#pragma once 

#include "iterator.h"

template<typename Type>
SetIterator<Type>::SetIterator(const SetIterator<Type>& iter)
{
    elements = iter.elements;
    index = iter.index;
    size = iter.size;
}

template<typename Type>
SetIterator<Type>::SetIterator(const std::shared_ptr<Type[]>& ptr, const long long& pos, const size_t& elemSize)
{   
    elements = ptr;
    index = pos;
    size = elemSize;
}

template<typename Type>
const Type* SetIterator<Type>::operator->() const
{
    std::shared_ptr<Type[]> local(elements);

    return local.get();
}

template<typename Type>
const Type& SetIterator<Type>::operator*() const
{
    if (!elements.lock())
    {
        throw (EmptyIteratorError("|Object was expired|"));
    }

    if (index < 0 || (size_t)index > size)
    {
        throw (IndexError("|Trying to dereference out of range element|", index));
    }

    std::shared_ptr<Type[]> local(elements);

    return *(local.get() + index);
}

template<typename Type>
SetIterator<Type>::operator bool() const
{
    std::shared_ptr<Type[]> local(elements);

    return (local.get() == nullptr);
}

template<typename Type>
size_t SetIterator<Type>::operator-(const SetIterator<Type>& iter) const
{
    if (this->index < iter.index)
    {
        throw (WrongConvertionError("|Negative result of operator-(const SetIterator<Type>& iter1, const SetIterator<Type>& iter2)|",
                                     (long long)this->index - (long long)iter.index));
    }

    return this->index - iter.index;
}

template<typename Type>
bool SetIterator<Type>::operator==(const SetIterator<Type>& iter) const
{
    return (this->index == iter.index);
}

template<typename Type>
bool SetIterator<Type>::operator!=(const SetIterator<Type>& iter) const
{
    return (this->index != iter.index);
}

template<typename Type>
bool SetIterator<Type>::operator<=(const SetIterator<Type>& iter) const
{
    return (this->index <= iter.index);
}

template<typename Type>
bool SetIterator<Type>::operator>=(const SetIterator<Type>& iter) const
{
    return (this->index >= iter.index);
}

template<typename Type>
bool SetIterator<Type>::operator<(const SetIterator<Type>& iter) const
{
    return (this->index < iter.index);
}

template<typename Type>
bool SetIterator<Type>::operator>(const SetIterator<Type>& iter) const
{
    return (this->index > iter.index);
}

template<typename Type>
SetIterator<Type>& SetIterator<Type>::operator-(const long long& delta)
{
    this->index -= delta;
    return *this;
}

template<typename Type>
SetIterator<Type>& SetIterator<Type>::operator+(const long long& delta)
{
    this->index += delta;
    return *this;
}

template<typename Type>
SetIterator<Type>& SetIterator<Type>::operator++()
{
    index++;
    return *this;
}

template<typename Type>
SetIterator<Type> SetIterator<Type>::operator++(int)
{
    SetIterator<Type> local(*this);

    ++(*this);

    return local;
}

template<typename Type>
SetIterator<Type>& SetIterator<Type>::operator--()
{
    index--;
    return *this;
}

template<typename Type>
SetIterator<Type> SetIterator<Type>::operator--(int)
{
    SetIterator<Type> local(*this);

    --(*this);

    return local;
}