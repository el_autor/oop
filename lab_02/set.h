#pragma once

#include <iostream>
#include <initializer_list>
#include <iterator>
#include <memory>
#include "set_base.h"
#include "iterator.hpp"
#include "exceptions.h"

template<typename Type>
class Set : public SetBase
{
public:

    Set();
    template<typename Iterator>
    Set(Iterator begin, Iterator end);
    Set(const Type* const arr, const size_t& arrSize);
    Set(const std::initializer_list<Type>& args);
    explicit Set(const Set<Type>& set);
    Set(Set<Type>&& set);

    void add(const Type& elem);
    void add(const Set<Type>& set);
    void add(const std::initializer_list<Type>& set);
    template<typename Iterator>
    void add(Iterator begin, Iterator end);

    void remove(const Type& elem);
    void remove(const Set<Type>& set);
    void remove(const std::initializer_list<Type>& set);
    template<typename Iterator>
    void remove(Iterator begin, Iterator end);

    bool isExist(const Type& elem) const;
    bool isExist(const Set<Type>& set) const;
    bool isExist(const std::initializer_list<Type>& set) const;
    template<typename Iterator>
    bool isExist(Iterator begin, Iterator end);

    bool isSubSet(const Set<Type>& set) const;
    bool isSubSet(SetIterator<Type> begin, SetIterator<Type> end) const;
    bool isSuperSet(const Set<Type>& set) const;
    bool isSuperSet(SetIterator<Type> begin, SetIterator<Type> end) const;
    bool operator<=(const Set<Type>& set) const;
    bool operator>=(const Set<Type>& set) const;
    bool operator<(const Set<Type>& set) const;
    bool operator>(const Set<Type>& set) const;

    bool equal(const Set<Type>& set) const;
    bool equal(SetIterator<Type> begin, SetIterator<Type> end) const;
    bool operator==(const Set<Type>& set) const;    
    bool operator!=(const Set<Type>& set) const;

    Set<Type> intersection(const Set<Type>& set) const;
    Set<Type> intersection(SetIterator<Type> begin, SetIterator<Type> end) const;
    Set<Type> operator*(const Set<Type>& set) const;
    Set<Type> unification(const Set<Type>& set) const;
    Set<Type> unification(SetIterator<Type> begin, SetIterator<Type> end) const;
    Set<Type> operator+(const Set<Type>& set) const;
    Set<Type> difference(const Set<Type>& set) const;
    Set<Type> difference(SetIterator<Type> begin, SetIterator<Type> end) const;
    Set<Type> operator-(const Set<Type>& set) const;

    Set<Type>& operator=(const Set<Type>& set);
    Set<Type>& operator=(Set<Type>&& set);
    Set<Type>& operator=(const std::initializer_list<Type>& set);

    void clear();

    SetIterator<Type> begin() const;
    SetIterator<Type> end() const;

private:

    bool _isSubSet_(SetIterator<Type> begin, SetIterator<Type> end) const;
    bool _isSuperSet_(SetIterator<Type> begin, SetIterator<Type> end) const;
    bool _equal_(SetIterator<Type> begin, SetIterator<Type> end) const;
    Set<Type> _intersection_(SetIterator<Type> begin, SetIterator<Type> end) const;
    Set<Type> _unification_(SetIterator<Type> begin, SetIterator<Type> end) const;
    Set<Type> _difference_(SetIterator<Type> begin, SetIterator<Type> end) const;
    long long binarySearch(const long long& low, const long long& high, const Type& elem) const;
    void insert(const Type& elem);
    void allocateMemory(const size_t& sizeToAlloc);

    std::shared_ptr<Type[]> elements;

};