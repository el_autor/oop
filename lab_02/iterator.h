#pragma once

#include <iterator>
#include <memory>
#include "exceptions.h"

template<typename Type>
class SetIterator : public std::iterator<std::bidirectional_iterator_tag, Type>
{
public:

    SetIterator() = delete;
    SetIterator(const std::shared_ptr<Type[]>& ptr, const long long& pos, const size_t& elemSize);
    SetIterator(const SetIterator<Type>& iter);
    
    const Type& operator*() const;
    const Type* operator->() const;
    operator bool() const;

    size_t operator-(const SetIterator<Type>& iter) const;
    bool operator==(const SetIterator<Type>& iter) const;
    bool operator!=(const SetIterator<Type>& iter) const;
    bool operator>=(const SetIterator<Type>& iter) const;
    bool operator<=(const SetIterator<Type>& iter) const;
    bool operator>(const SetIterator<Type>& iter) const;
    bool operator<(const SetIterator<Type>& iter) const;

    SetIterator<Type>& operator-(const long long& delta);
    SetIterator<Type>& operator+(const long long& delta);
    SetIterator<Type>& operator++();
    SetIterator<Type> operator++(int);
    SetIterator<Type>& operator--();
    SetIterator<Type> operator--(int);

private:

    std::weak_ptr<Type[]> elements;
    long long index;
    size_t size;

};