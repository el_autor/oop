#include "make_scale.h"

int scale_tmp_model(Model& tmpModel, const ScaleData& handler)
{
    MatrixAgent agent = init_matrix_agent();
    int error = create_matrices(agent);        

    if (!error)
    {
        add_scale_handler_values(agent.transformationMatrix, handler);
        transform_model(tmpModel.currentState, agent);
    }

    if (error != OK && error != MATRIX_NOT_ALLOCATED)
    {
        free_matrices(agent);
    }

    return error;
}

int make_scale(Model& model, const ScaleData& handler)
{
    int error = model_was_loaded(model);
    Model tmpModel = init_model();

    if (!error)
    {
        error = copy_model(tmpModel, model); 
    }

    ShiftData moveHandler = init_move_handler();

    if (!error)
    {
        set_move_values(moveHandler, tmpModel.currentState, true);
        error = make_shift(tmpModel, moveHandler);
    }

    if (!error)
    {
        error = scale_tmp_model(tmpModel, handler);
    }

    if (!error)
    {
        inverse_move_values(moveHandler);
        error = make_shift(tmpModel, moveHandler);
    }

    if (!error)
    {
        load_new_state(model, tmpModel);
    }
    else
    {
        free_model(tmpModel);
    }

    return error;
}