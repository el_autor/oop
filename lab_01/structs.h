#pragma once

#include <iostream>
#include <cmath>
#include "consts.h"

struct Point
{
    double x = 0, y = 0, z = 0;
};

struct Connection
{
    int leftPoint = 0, rightPoint = 0;
};

struct ProectionPoint
{
    double x = 0, y = 0;
};

struct Edge
{
    ProectionPoint left, right;
};

struct ShiftData
{
    double dx = 0, dy = 0, dz = 0;
};

struct ScaleData
{
    double kx = 1, ky = 1, kz = 1;
};

struct RotateData
{
    int angleX = 0, angleY = 0, angleZ = 0;
};

struct ProectionData
{
    Edge *edges = NULL;
    int edgesNumber = 0;
};

struct LoadData
{
    char *sourceFilename = NULL;
};

struct DataAgent
{
    ShiftData shiftHandler;
    ScaleData scaleHandler;
    RotateData rotateHandler;
    ProectionData proectionHandler;
    LoadData loadHandler;
};