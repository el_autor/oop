#pragma once

#include "structs.h"
#include "matrix_operations.h"
#include "consts.h"

using namespace std;

struct StateData
{
    Point *currentState = NULL;
    int pointsNumber = 0;
};

struct ConnectionData
{
    Connection *connections = NULL;
    int connectionsNumber = 0;
};

struct Model
{
    StateData currentState;
    ConnectionData connections;
    bool modelIsNew = true;
};

Model init_model();
Connection init_connection();
ConnectionData init_connections();
Point init_point();
StateData init_state();
int model_was_loaded(const Model& model);
void free_model(Model& model);
void free_state(StateData& data);
void free_connections(ConnectionData& data);
void load_new_state(Model& srcModel, const Model& tmpModel);
int copy_model(Model& destModel, const Model& srcModel);
void transform_model(StateData& currentState, MatrixAgent& agent);