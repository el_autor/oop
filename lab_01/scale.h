#pragma once

#include "structs.h"
#include "matrix_operations.h"

void add_scale_handler_values(Matrix& scaleMatrix, const ScaleData& handler);