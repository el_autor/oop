#pragma once

#include "structs.h"
#include "consts.h"
#include "matrix_operations.h"
#include "model_operations.h"
#include "shift.h"

int make_shift(Model& model, const ShiftData& handler);
ShiftData init_move_handler();
void inverse_move_values(ShiftData& handler);
void set_move_values(ShiftData& handler, const StateData& data, const bool& inverse);