#include "load_model.h"

int load_tmp_model(Model& tmpModel, FILE *const sourceStream)
{
    StateData tmpCurrentState = init_state();
    int error = load_points(tmpCurrentState, sourceStream);
    ConnectionData tmpConnections = init_connections();

    if (!error)
    {
        if ((error = load_connections(tmpConnections, sourceStream)))
        {
            free_state(tmpCurrentState);
        }
    }

    if (!error)
    {
        tmpModel.currentState = tmpCurrentState;
        tmpModel.connections = tmpConnections;
    }

    return error;
}

void reset_model(Model& destModel, const Model& tmpModel)
{
    free_model(destModel);
    destModel = tmpModel;
}

int load_model(Model& model, const LoadData& loadHandler)
{
    int error = OK;

    if (!loadHandler.sourceFilename)
    {
        error = FILENAME_NOT_LOADED;
    }

    FILE *source = NULL;

    if (!error && !(source = fopen(loadHandler.sourceFilename, "r")))
    {
        error = FILE_NOT_OPENED; 
    }

    Model tmpModel = init_model();

    if (!error)
    {
        error = load_tmp_model(tmpModel, source);
    }

    if (error != FILENAME_NOT_LOADED)
    {
        fclose(source);
    }

    if (!error)
    {
        reset_model(model, tmpModel);
    }

    return error;
}