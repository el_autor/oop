#include "connections.h"

int scanf_connections(Connection *connections, const int& size, FILE *const source)
{
    int error = OK;

    if (!connections)
    {
        error = MEMORY_NOT_ALLOCATED;
    }
    else
    {
        for (int i = 0; i < size && !error; i++)
        {
            if (fscanf(source, "%d %d", &(connections[i].leftPoint), &(connections[i].rightPoint)) != 2)
            {
                error = WRONG_DATA_INPUT;
            }
        }
    }

    return error;
}

int load_connections(ConnectionData& data, FILE *const sourceStream)
{
    int error = OK, connectionsNumber = 0;

    if (fscanf(sourceStream, "%d", &connectionsNumber) != 1)
    {
        error = WRONG_DATA_INPUT;
    }

    Connection *connections = NULL;

    if (!error && !(connections = (Connection *)(malloc(sizeof(Connection) * (connectionsNumber)))))
    {
        error = MEMORY_NOT_ALLOCATED;
    }

    if (!error)
    {
        error = scanf_connections(connections, connectionsNumber, sourceStream);
    }

    if (!error)
    {
        data.connections = connections;
        data.connectionsNumber = connectionsNumber;
    }
    else
    {
        free(connections);
    }

    return error;
}