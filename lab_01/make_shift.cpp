#include "make_shift.h"

int shift_tmp_model(Model& tmpModel, const ShiftData& handler)
{
    MatrixAgent agent = init_matrix_agent();

    int error = create_matrices(agent);        

    if (!error)
    {
        add_shift_handler_values(agent.transformationMatrix, handler);
        transform_model(tmpModel.currentState, agent);
    }

    if (error != OK && error != MATRIX_NOT_ALLOCATED)
    {
        free_matrices(agent);
    }

    return error;
}

int make_shift(Model& model, const ShiftData& handler)
{
    int error = model_was_loaded(model);
    Model tmpModel = init_model();

    if (!error)
    {
        error = copy_model(tmpModel, model);
    }

    if (!error)
    {
        error = shift_tmp_model(tmpModel, handler);
    }

    if (!error)
    {
        load_new_state(model, tmpModel);
    }
    else
    {
        free_model(tmpModel);
    }

    return error;
}