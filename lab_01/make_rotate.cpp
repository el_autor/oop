#include "make_rotate.h"

int rotate_tmp_model(Model& tmpModel, const RotateData& handler)
{
    MatrixAgent agent = init_matrix_agent();

    int error = create_matrices(agent);        

    if (!error)
    {
        add_rotate_handler_values_z(agent.transformationMatrix, handler);
        transform_model(tmpModel.currentState, agent);

        add_rotate_handler_values_x(agent.transformationMatrix, handler);
        transform_model(tmpModel.currentState, agent);

        add_rotate_handler_values_y(agent.transformationMatrix, handler);
        transform_model(tmpModel.currentState, agent);
    }

    if (error != MATRIX_NOT_ALLOCATED)
    {
        free_matrices(agent);
    }

    return error;
}

int make_rotate(Model& model, const RotateData& handler)
{
    int error = model_was_loaded(model);
    Model tmpModel = init_model();

    if (!error)
    {
        error = copy_model(tmpModel, model); 
    }

    ShiftData moveHandler = init_move_handler();

    if (!error)
    {
        set_move_values(moveHandler, tmpModel.currentState, true);
        error = make_shift(tmpModel, moveHandler);
    }

    if (!error)
    {
        error = rotate_tmp_model(tmpModel, handler);
    }

    if (!error)
    {
        inverse_move_values(moveHandler);
        error = make_shift(tmpModel, moveHandler);
    }

    if (!error)
    {
        load_new_state(model, tmpModel);
    }
    else
    {
        free_model(tmpModel);
    }

    return error;
}