#pragma once

#include "structs.h"

using namespace std;

struct Matrix
{
    double **data = NULL;
    int rows = 0, cols = 0;
};

struct MatrixAgent
{
    Matrix transformationMatrix, oldPoint, newPoint;
};

MatrixAgent init_matrix_agent();
int fill_point_matrix(Matrix& dest, const Point& src);
int multiply(Matrix& result, const Matrix& point, const Matrix& converter);
int reset_point(Point& destination, const Matrix& src);
int make_default_matrix(Matrix& matrix);
int create_matrices(MatrixAgent& agent);
void free_matrices(MatrixAgent& agent);