#pragma once

#include "structs.h"
#include "matrix_operations.h"
#include "model_operations.h"
#include "make_shift.h"
#include "rotate.h"

int make_rotate(Model& model, const RotateData& handler);