#include "task_manager.h"

int call_actions(DataAgent& data, const int& key)
{
    static Model model = init_model();
    int error = OK;

    switch (key)
    {
        case (Commands::LoadModel):

            error = load_model(model, data.loadHandler);

            break;

        case (Commands::GetProection):

            error = get_proection(data.proectionHandler, model);

            break;

        case (Commands::Move):

            error = make_shift(model, data.shiftHandler);

            break;

        case (Commands::Scale):

            error = make_scale(model, data.scaleHandler);

            break;

        case (Commands::Rotate):

            error = make_rotate(model, data.rotateHandler);

            break;

        case (Commands::Clean):
        
            free_model(model);

            break;

        default:

            error = UNKNOWN_COMMAND;  
    }

    return error;
}