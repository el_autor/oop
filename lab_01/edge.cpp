#include "edge.h"

void load_endpoint(ProectionPoint& dest_point, const Point& src_point)
{
    dest_point.x = src_point.x;
    dest_point.y = src_point.y;
}

int load_single_proection(Edge& dest_edge, const Connection& connection, const Point *const curState)
{
    int error = OK;

    if (!curState)
    {
        error = MEMORY_NOT_ALLOCATED;
    }
    else
    {
        int index = 0;
        
        index = connection.leftPoint;
        load_endpoint(dest_edge.left, curState[index]);
        index = connection.rightPoint;
        load_endpoint(dest_edge.right, curState[index]);
    }

    return error;
}