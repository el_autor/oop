#pragma once

#include "structs.h"
#include "consts.h"
#include "model_operations.h"
#include "points.h"
#include "connections.h"

using namespace std;

Model init_model();
StateData init_state();
ConnectionData init_connections();
int load_model(Model& model, const LoadData& loadHandler);