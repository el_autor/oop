#pragma once

#include "structs.h"
#include "model_operations.h"

int load_points(StateData& data, FILE *const sourceStream);