#include "points.h"

void reload_minimax(double& min, double& max, const double& value)
{
    if (value < min)
    {
        min = value;
    }
    else if (value > max)
    {
        max = value;
    }
}

int add_center_point(Point *const curState, const int& size)
{
    int error = OK;

    if (curState)
    {
        double xMin = curState[0].x, xMax = curState[0].x,
               yMin = curState[0].y, yMax = curState[0].y,
               zMin = curState[0].z, zMax = curState[0].y;
 
        for (int i = 1; i < size; i++)
        {
            reload_minimax(xMin, xMax, curState[i].x);
            reload_minimax(yMin, yMax, curState[i].y);
            reload_minimax(zMin, zMax, curState[i].z);
        }

        curState[size].x = (xMin + xMax) / 2;
        curState[size].y = (yMin + yMax) / 2;
        curState[size].z = (zMin + zMax) / 2;
    }
    else
    {
        error = MEMORY_NOT_ALLOCATED;
    }

    return error; 
}

int scanf_points(Point *points, const int& size, FILE *const source)
{
    int error = OK;

    if (!points)
    {
        error = MEMORY_NOT_ALLOCATED;
    }
    else
    {
        for (int i = 0; i < size && !error; i++)
        {
            if (fscanf(source, "%lf %lf %lf", &(points[i].x), &(points[i].y), &(points[i].z)) != 3)
            {
                error = WRONG_DATA_INPUT;
            }
        }
    }

    return error;
}

int load_points(StateData& data, FILE *const sourceStream)
{
    int error = OK, pointsNumber = 0;

    if (fscanf(sourceStream, "%d", &pointsNumber) != 1)
    {   
        error = WRONG_DATA_INPUT;
    }

    Point* currentState = NULL;

    if (!error && !(currentState = (Point *)(malloc(sizeof(Point) * (pointsNumber + 1)))))
    {
        error = MEMORY_NOT_ALLOCATED;
    }

    if (!error)
    {
        error = scanf_points(currentState, pointsNumber, sourceStream);
    }

    if (!error)
    {
        error = add_center_point(currentState, pointsNumber);
    }

    if (!error)
    {
        data.pointsNumber = pointsNumber;
        data.currentState = currentState;
    }
    else
    {
        free(currentState);
    }

    return error;
}