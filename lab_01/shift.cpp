#include "shift.h"

ShiftData init_move_handler()
{
    ShiftData data;

    data.dx = 0;
    data.dy = 0;
    data.dz = 0;

    return data;
}

void set_move_values(ShiftData& handler, const StateData& data, const bool& inverse)
{
    Point centerPoint = data.currentState[data.pointsNumber];

    handler.dx = -centerPoint.x;
    handler.dy = centerPoint.y;
    handler.dz = -centerPoint.z;
}

void inverse_move_values(ShiftData& handler)
{
    handler.dx *= -1;
    handler.dy *= -1;
    handler.dz *= -1;
}

void add_shift_handler_values(Matrix& shiftMatrix, const ShiftData& handler)
{
    make_default_matrix(shiftMatrix);

    shiftMatrix.data[3][0] = handler.dx;
    shiftMatrix.data[3][1] = -handler.dy;
    shiftMatrix.data[3][2] = handler.dz;
}