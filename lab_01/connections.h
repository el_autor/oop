#pragma once

#include "structs.h"
#include "model_operations.h"

int load_connections(ConnectionData& data, FILE *const sourceStream);