#pragma once

#include "structs.h"
#include "matrix_operations.h"

void add_rotate_handler_values_z(Matrix& rotateMatrix, const RotateData& handler);
void add_rotate_handler_values_x(Matrix& rotateMatrix, const RotateData& handler);
void add_rotate_handler_values_y(Matrix& rotateMatrix, const RotateData& handler);