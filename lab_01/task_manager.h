#pragma once

#include "structs.h"
#include "load_model.h"
#include "make_shift.h"
#include "make_scale.h"
#include "make_rotate.h"
#include "get_proection.h"
#include "consts.h"
#include "model_operations.h"

enum Commands
{
    LoadModel,
    Move,
    Scale,
    Rotate,
    GetProection,
    Clean,
};

int call_actions(DataAgent& data, const int& key);