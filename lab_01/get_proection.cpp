#include "get_proection.h"

ProectionData init_proection()
{
    ProectionData data;

    data.edges = NULL;
    data.edgesNumber = 0;

    return data;
}

int load_tmp_values(ProectionData& proection, const StateData& currentState, const ConnectionData& connections)
{
    int error = OK;

    if (!proection.edges || !currentState.currentState || !connections.connections)
    {
        error = MEMORY_NOT_ALLOCATED;
    }

    int connections_size = connections.connectionsNumber;

    if (!error)
    {
        for (int i = 0; i < connections_size; i++)
        {
            load_single_proection(proection.edges[i], connections.connections[i], currentState.currentState);
        }
    }

    return error;
}

int setup_tmp_proection(ProectionData& tmpProection, const StateData& currentState, const ConnectionData& connections)
{
    int error = 0, size = connections.connectionsNumber;
    Edge *edges = NULL;

    if (!(edges = (Edge *)(malloc(sizeof(Edge) * size))))
    {
        error = PROECTION_NOT_SETUP;
    }

    if (!error)
    {
        tmpProection.edgesNumber = size;
        tmpProection.edges = edges;
    }

    if (!error)
    {
        load_tmp_values(tmpProection, currentState, connections);
    }

    return error;
}

int reload_tmp_proection(ProectionData& tmpProeciton, const StateData& currentState, const ConnectionData& connections)
{
    return load_tmp_values(tmpProeciton, currentState, connections);;
}
   
void reset_proection(ProectionData& destProection, bool& modelIsNew, ProectionData& srcProection)
{
    if (modelIsNew)
    {
        free_proection(destProection);
        destProection = srcProection;
        modelIsNew = false;
    }
}

int get_proection(ProectionData& proection, Model& model)
{
    int error = model_was_loaded(model);
    ProectionData tmp_proection = init_proection();

    if (!error) 
    {
        tmp_proection = proection;
    }

    if (!error)
    {
        if (model.modelIsNew)
        {
            error = setup_tmp_proection(tmp_proection, model.currentState, model.connections);
        }
        else
        {
            error = reload_tmp_proection(tmp_proection, model.currentState, model.connections);
        }
    }

    if (!error)
    {
        reset_proection(proection, model.modelIsNew, tmp_proection);
    }

    return error;
}