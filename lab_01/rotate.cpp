#include "rotate.h"

void add_rotate_handler_values_z(Matrix& rotateMatrix, const RotateData& handler)
{
    double arg = (handler.angleZ / 180.0) * M_PI;

    make_default_matrix(rotateMatrix);

    rotateMatrix.data[0][0] = cos(arg);
    rotateMatrix.data[1][1] = cos(arg);
    rotateMatrix.data[0][1] = sin(arg);
    rotateMatrix.data[1][0] = -sin(arg);
}

void add_rotate_handler_values_x(Matrix& rotateMatrix, const RotateData& handler)
{
    double arg = (handler.angleX / 180.0) * M_PI;

    make_default_matrix(rotateMatrix);

    rotateMatrix.data[1][1] = cos(arg);
    rotateMatrix.data[2][2] = cos(arg);
    rotateMatrix.data[1][2] = sin(arg);
    rotateMatrix.data[2][1] = -sin(arg);   
}

void add_rotate_handler_values_y(Matrix& rotateMatrix, const RotateData& handler)
{
    double arg = (handler.angleY / 180.0) * M_PI;

    make_default_matrix(rotateMatrix);

    rotateMatrix.data[0][0] = cos(arg);
    rotateMatrix.data[2][2] = cos(arg);
    rotateMatrix.data[2][0] = sin(arg);
    rotateMatrix.data[0][2] = -sin(arg);    
}