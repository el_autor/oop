#pragma once

#include "structs.h"
#include "consts.h"
#include "model_operations.h"
#include "edge.h"
#include "free_proection.h"

using namespace std;

int get_proection(ProectionData& data, Model& model);
int model_was_loaded(const Model& model);