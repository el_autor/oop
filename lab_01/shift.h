#pragma once

#include "structs.h"
#include "matrix_operations.h"
#include "model_operations.h"

void add_shift_handler_values(Matrix& shiftMatrix, const ShiftData& handler);
void inverse_move_values(ShiftData& handler);
void set_move_values(ShiftData& handler, const StateData& data, const bool& inverse);
ShiftData init_move_handler();