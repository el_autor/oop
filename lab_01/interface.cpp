#include "interface.h"

LoadData init_load_handler()
{
    LoadData data;

    data.sourceFilename = NULL;

    return data;
}

ProectionData init_proection_handler()
{
    ProectionData data;

    data.edges = NULL;
    data.edgesNumber = 0;

    return data;
}

RotateData init_rotate_handler()
{
    RotateData data;

    data.angleX = 0;
    data.angleY = 0;
    data.angleZ = 0;

    return data;
}

ScaleData init_scale_handler()
{
    ScaleData data;

    data.kx = 0;
    data.ky = 0;
    data.kz = 0;

    return data;
}

ShiftData init_shift_handler()
{
    ShiftData data;

    data.dx = 0;
    data.dy = 0;
    data.dz = 0;

    return data;
}

DataAgent init_data()
{
    DataAgent data;

    data.loadHandler = init_load_handler();
    data.proectionHandler = init_proection_handler();
    data.rotateHandler = init_rotate_handler();
    data.scaleHandler = init_scale_handler();
    data.shiftHandler = init_shift_handler();

    return data;
}

void App::show_message_by_error(const int& errCode)
{
    cout << errCode << endl;

    switch (errCode)
    {
        case MEMORY_NOT_ALLOCATED:

            signal.get_message_signal().emit("Не удалось выделить достаточно памяти!");    
            break;

        case BACK_DATA_NOT_ALLOCATED:

            signal.get_message_signal().emit("Не удалось выделить память под данные для рисования модели!");
            break;

        case MATRIX_NOT_ALLOCATED:

            signal.get_message_signal().emit("Не удалось выделить память под матрицы!");
            break;

        case MATRIX_MULTIPLY_NOT_AVAILABLE:

            signal.get_message_signal().emit("Неверные размерности матриц при умножении!");
            break;

        case FILE_NOT_OPENED:

            signal.get_message_signal().emit("Файл не может быть открыт!");
            break;

        case FILENAME_NOT_LOADED:

            signal.get_message_signal().emit("Память под имя файла не выделена!");
            break;

        case WRONG_DATA_INPUT:

            signal.get_message_signal().emit("Ошибка ввода в файле!");
            break;

        case WRONG_MOVE_DELTA:

            signal.get_message_signal().emit("Введите вещественные dx, dy, dz!");
            break;

        case WRONG_SCALE_KOEFFS:

            signal.get_message_signal().emit("Введите вещественные kx, ky, kz!");
            break;

        case WRONG_ROTATE_ANGLES:

            signal.get_message_signal().emit("Введите угол в градусах!");
            break;
    }
}

sigc::signal<void, const string&> Signal::get_message_signal()
{
    return message_signal;
}

void App::on_show_message(const string& message)
{
    Gtk::MessageDialog dialog(*(this->mainWindow), "Ошибка!");

    dialog.set_secondary_text(message);
    dialog.run();
}

void App::on_moveButton_clicked()
{
    int error = OK;
    ShiftData& handler = data.shiftHandler;

    try
    {
        handler.dx = std::stod(dxEntry->get_text());
        handler.dy = std::stod(dyEntry->get_text());
        handler.dz = std::stod(dzEntry->get_text());    

        error = call_actions(data, Commands::Move);
        
        if (error)
        {
            show_message_by_error(error);
        }
        else
        {
            canvas->queue_draw();
        }
    }
    catch (invalid_argument const &error)
    {
        show_message_by_error(WRONG_MOVE_DELTA);
    }
}

void App::on_scaleButton_clicked()
{
    int error = OK;
    ScaleData& handler = data.scaleHandler;
    
    try
    {
        handler.kx = std::stod(kxEntry->get_text());
        handler.ky = std::stod(kyEntry->get_text());
        handler.kz = std::stod(kzEntry->get_text());

        error = call_actions(data, Commands::Scale);

        if (error)
        {
            show_message_by_error(error);
        }
        else
        {
            canvas->queue_draw();
        }
    }
    catch (invalid_argument const &error)
    {
        show_message_by_error(WRONG_SCALE_KOEFFS);
    }
}

void App::on_rotateButton_clicked()
{   
    int error = OK;
    RotateData& handler = data.rotateHandler;

    try
    {
        handler.angleX = std::stoi(angleEntryX->get_text());
        handler.angleY = std::stoi(angleEntryY->get_text());
        handler.angleZ = std::stoi(angleEntryZ->get_text());
        
        error = call_actions(data, Commands::Rotate);

        if (error)
        {
            show_message_by_error(error);
        }
        else
        {
            canvas->queue_draw(); 
        }
    }
    catch (invalid_argument const &error)
    {
        show_message_by_error(WRONG_ROTATE_ANGLES);
    }
}

static char *string_to_char(const string& str)
{
    char *newString = NULL;

    newString = (char *)(malloc(sizeof(char) * (str.size() + 1)));

    if (newString)
    {
        for (unsigned i = 0; i < str.size(); i++)
        {
            newString[i] = str[i];        
        }
    }

    newString[str.size()] = '\0';

    return newString;
}

void App::on_inputButton_clicked()
{
    int error = OK;

    data.loadHandler.sourceFilename = string_to_char(filenameEntry->get_text());

    if ((error = call_actions(data, Commands::LoadModel)))
    {
        show_message_by_error(error); 
    } 
    else
    {
        modelIsLoaded = true;
    }

    free(data.loadHandler.sourceFilename);
    canvas->queue_draw();
}

bool App::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    int error = OK, edgesNumber = 0;
    Edge *edges = NULL;

    if (this->modelIsLoaded)
    {
        error = call_actions(data, Commands::GetProection);    
    }

    edges = data.proectionHandler.edges;
    edgesNumber =  data.proectionHandler.edgesNumber;

    if (!error)
    {
        cr->set_line_width(2.0);
        cr->set_source_rgb(0.0, 0.0, 0.8);

        for (int i = 0; i < edgesNumber; i++)
        {
            cr->move_to(edges[i].left.x, edges[i].left.y);
            cr->line_to(edges[i].right.x, edges[i].right.y);
        }

        cr->stroke();
    }
    else
    {
        show_message_by_error(error);
    }

    return true;
}

App::App()
{
    _builder_ = Gtk::Builder::create();

    _builder_->add_from_file("lab_01.glade");
    _builder_->get_widget("mainWindow", mainWindow);
    mainWindow->set_title("Lab_01");

    _builder_->get_widget("moveButton", moveButton);
    _builder_->get_widget("scaleButton", scaleButton);
    _builder_->get_widget("rotateButton", rotateButton);
    _builder_->get_widget("inputButton", inputButton);

    _builder_->get_widget("dxEntry", dxEntry);
    _builder_->get_widget("dyEntry", dyEntry);
    _builder_->get_widget("dzEntry", dzEntry);
    _builder_->get_widget("kxEntry", kxEntry);
    _builder_->get_widget("kyEntry", kyEntry);
    _builder_->get_widget("kzEntry", kzEntry);
    _builder_->get_widget("angleEntryX", angleEntryX);
    _builder_->get_widget("angleEntryY", angleEntryY);
    _builder_->get_widget("angleEntryZ", angleEntryZ);
    _builder_->get_widget("filenameEntry", filenameEntry);

    _builder_->get_widget("canvas", canvas);

    moveButton->signal_clicked().connect(sigc::mem_fun(*this, &App::on_moveButton_clicked));
    scaleButton->signal_clicked().connect(sigc::mem_fun(*this, &App::on_scaleButton_clicked));
    rotateButton->signal_clicked().connect(sigc::mem_fun(*this, &App::on_rotateButton_clicked));
    inputButton->signal_clicked().connect(sigc::mem_fun(*this, &App::on_inputButton_clicked));
    canvas->signal_draw().connect(sigc::mem_fun(*this, &App::on_draw));
    signal.get_message_signal().connect(sigc::mem_fun(*this, &App::on_show_message));

    data = init_data();
    modelIsLoaded = false;
}

App::~App()
{
    call_actions(data, Commands::Clean);
    free_proection(data.proectionHandler);
}

Gtk::Window*& App::get_window()
{
    return mainWindow;
}