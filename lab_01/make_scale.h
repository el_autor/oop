#pragma once

#include "structs.h"
#include "matrix_operations.h"
#include "model_operations.h"
#include "make_shift.h"
#include "scale.h"

int make_scale(Model& model, const ScaleData& handler);