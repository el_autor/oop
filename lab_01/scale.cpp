#include "scale.h"

void add_scale_handler_values(Matrix& scaleMatrix, const ScaleData& handler)
{
    make_default_matrix(scaleMatrix);

    scaleMatrix.data[0][0] = handler.kx;
    scaleMatrix.data[1][1] = handler.ky;
    scaleMatrix.data[2][2] = handler.kz;
}