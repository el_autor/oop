#include "matrix_operations.h"

void free_matrix(Matrix& matrix)
{
    int rows = matrix.rows;
    double **data = matrix.data;

    if (data)
    {
        for (int i = 0; i < rows; i++)
        {   
            free(data[i]);
        }

        free(data);
        data = NULL;
    }
}

int create_matrix(Matrix& matrix)
{
    int error = OK;
    double **data = NULL;

    if (!(data = (double **)(malloc(sizeof(double *) * matrix.rows))))
    {
        error = MEMORY_NOT_ALLOCATED;
    }

    for (int i = 0; i < matrix.rows && !error; i++)
    {
        if (!(data[i] = (double *)(malloc(sizeof(double *) * matrix.cols))))
        {
            matrix.data = data;
            free_matrix(matrix);
            error = MEMORY_NOT_ALLOCATED;
        }
    }

    if (!error)
    {
        matrix.data = data;
    }

    return error;
}

int multiply(Matrix& result, const Matrix& point, const Matrix& converter)
{
    int error = OK;
    double **resultData = result.data,
           **pointData = point.data,
           **converterData = converter.data;

    if (!resultData || !pointData || !converterData)
    {
        error = MATRIX_NOT_ALLOCATED;
    }
    
    int pointRows = point.rows, converterCols = converter.cols, pointCols = point.cols;

    if (!error && (pointCols != converter.rows || result.cols != pointCols || result.rows != pointRows))
    {
        error = MATRIX_MULTIPLY_NOT_AVAILABLE;
    }

    if (!error)
    {
        for (int i = 0; i < pointRows; i++)
        {
            for (int j = 0; j < converterCols; j++)
            {
                double cell = 0; 

                for (int k = 0; k < pointCols; k++)
                {
                    cell += pointData[i][k] * converterData[k][j];
                }

                resultData[i][j] = cell;
            }
        }
    }

    return error;
}

int make_default_matrix(Matrix& matrix)
{
    int error = OK;

    if (!matrix.data)
    {
        error = MATRIX_NOT_ALLOCATED;
    }

    if (!error)
    {
        for (int i = 0; i < matrix.rows; i++)
        {
            matrix.data[i][i] = 1;

            for (int j = 0; j < matrix.cols; j++)
            {
                if (i != j)
                {
                    matrix.data[i][j] = 0;
                }
            }
        }
    }

    return error;    
}

int fill_point_matrix(Matrix& dest, const Point& src)
{
    int error = OK;
    double **destData = dest.data;

    if (!destData)
    {
        error = MATRIX_NOT_ALLOCATED;
    }
    else
    {
        destData[0][0] = src.x;
        destData[0][1] = src.y;
        destData[0][2] = src.z;
        destData[0][3] = 1;
    }

    return error;
}

int reset_point(Point& destination, const Matrix& src)
{
    int error = OK;
    double **srcData = src.data;

    if (!srcData)
    {
        error = MEMORY_NOT_ALLOCATED;
    }
    else
    {
        destination.x = srcData[0][0];
        destination.y = srcData[0][1];
        destination.z = srcData[0][2];
    }

    return error;
}

void free_matrices(MatrixAgent& agent)
{
    free_matrix(agent.transformationMatrix);
    free_matrix(agent.oldPoint);
    free_matrix(agent.newPoint);
}

int create_matrices(MatrixAgent& agent)
{
    int error = OK;

    if (create_matrix(agent.transformationMatrix) != OK ||
        create_matrix(agent.oldPoint) != OK ||
        create_matrix(agent.newPoint) != OK)
    {
        error = MATRIX_NOT_ALLOCATED;
        free_matrices(agent);
    }

    return error;
}

Matrix init_matrix(const int& rows, const int& cols)
{
    Matrix matrix;

    matrix.rows = rows;
    matrix.cols = cols;
    matrix.data = NULL;

    return matrix;
}

MatrixAgent init_matrix_agent()
{
    MatrixAgent agent;

    agent.transformationMatrix = init_matrix(ACTION_MATRIX_ROWS, ACTION_MATRIX_COLS);
    agent.oldPoint = init_matrix(1, ACTION_MATRIX_COLS);
    agent.newPoint = init_matrix(1, ACTION_MATRIX_COLS);

    return agent;
}