#include "model_operations.h"

Connection init_connection()
{
    Connection connection;

    connection.leftPoint = 0;
    connection.rightPoint = 0;

    return connection;    
}

ConnectionData init_connections()
{
    ConnectionData data;

    data.connections = NULL;
    data.connectionsNumber = 0;

    return data;
}

void free_connections(ConnectionData& data)
{
    free(data.connections);
    data.connections = NULL;
}

bool connections_was_loaded(const ConnectionData& data)
{
    return (bool)data.connections;
}

int copy_connection_data(ConnectionData& destData, const ConnectionData& srcData) // перенести
{
    int error = OK, size = srcData.connectionsNumber;
    Connection *data = NULL;

    if (!(data = (Connection *)(malloc(sizeof(Connection) * size))))
    {
        error = MEMORY_NOT_ALLOCATED;
    }
    else
    {
        for (int i = 0; i < size; i++)
        {
            data[i] = srcData.connections[i];
        }

        destData.connections = data;
        destData.connectionsNumber = size;
    }

    return error;
}

StateData init_state()
{
    StateData data;

    data.currentState = NULL;
    data.pointsNumber = 0;

    return data;
}

Point init_point()
{
    Point point;

    point.x = 0;
    point.y = 0;
    point.z = 0;

    return point;
}

Model init_model()
{
    Model model;

    model.currentState = init_state();
    model.connections = init_connections();
    model.modelIsNew = true;

    return model;
}

void free_state(StateData& data)
{
    free(data.currentState);
    data.currentState = NULL;
}

void free_model(Model& model)
{
    free_state(model.currentState);
    free_connections(model.connections);
}

bool points_was_loaded(const StateData& data)
{
    return (bool)data.currentState;
}

int model_was_loaded(const Model& model)
{
    return (!points_was_loaded(model.currentState) || !connections_was_loaded(model.connections));
}

int copy_state(StateData& destData, const StateData& srcData) // перенести 
{
    int error = OK, size = srcData.pointsNumber;
    Point *data = NULL;

    if (!(data = (Point *)(malloc(sizeof(Point) * (size + 1)))))
    {
        error = MEMORY_NOT_ALLOCATED;
    }
    else
    {
        for (int i = 0; i <= size; i++)
        {
            data[i] = srcData.currentState[i];
        }

        destData.currentState = data;
        destData.pointsNumber = size;
    }

    return error;
}

int copy_model(Model& destModel, const Model& srcModel)
{
    StateData tmp_current_state = init_state();
    int error = copy_state(tmp_current_state, srcModel.currentState);
    ConnectionData tmp_connection_data = init_connections();

    if (!error)
    {
        if ((error = copy_connection_data(tmp_connection_data, srcModel.connections)))
        {
            free_state(tmp_current_state);
        }
    }

    if (!error)
    {
        destModel.currentState = tmp_current_state;
        destModel.connections = tmp_connection_data;
        destModel.modelIsNew = srcModel.modelIsNew;
    }

    return error;
}

void load_new_state(Model& srcModel, const Model& tmpModel)
{
    free_state(srcModel.currentState);
    free_connections(srcModel.connections);
    srcModel = tmpModel;
}

void transform_model(StateData& currentState, MatrixAgent& agent)
{
    int pointsNumber = currentState.pointsNumber;

    for (int i = 0; i <= pointsNumber; i++)
    {
        fill_point_matrix(agent.oldPoint, currentState.currentState[i]);
        multiply(agent.newPoint, agent.oldPoint, agent.transformationMatrix);
        reset_point(currentState.currentState[i], agent.newPoint);
    }
}