#include "free_proection.h"

void free_proection(ProectionData& data)
{
    free(data.edges);
    data.edges = NULL;
}